/**************************************************************************
          pmc.c  -  Main file for PMC algorithm
          --------------------------------------------------------
    begin                : Sept 25 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "pmc.h"

#ifdef OPENCL
cl_context ctx;
cl_command_queue queue;
cl_device_id device;
long long devtype;
cl_uint computeunits;
cl_mem cled, cllen, clvres, clvsum, clvind;
cl_kernel knl_ReduceTwoStep;
size_t ldim[1];
size_t gdim[1];
cl_kernel *knl_VChunk, *knl_VChunkAdd;
char *knl_text;
cl_mem **clchunks;
size_t oldlen = 0;
char growing = 0;
char fastcl = 0;
#endif

FILE *outfile;
const char *argp_program_version =
  "pmc 0.1";
const char *argp_program_bug_address =
  "<andrea_corallo@yahoo.it>";
/* Program documentation. */
static char doc[] =
  "pmc take the dataset discretized form standard input or file and"
  " extract rules";
/* Description of the arguments we accept. */
static char args_doc[] = "[FILE]";
/* Options we understand. */
static struct argp_option options[] = {
  {"probability",  'p', "NUM",      0,  "The rule detection probability. Default value: "
   STR(RULEPROBDECT)},
  {"verbose",  'v', 0,      0,  "Produce verbose output" },
  {"interactve", 'i', 0,      0, "Interactive prompt" },
  {"autodim", 'a', 0,      0, "Find dinamicaly the max number of "
   "conditions for the rules." },
  {"quiet",    'q', 0,      0,  "Don't produce any progress output" },
  {"longnames",    'l', 0,      0,  "Produce rules extended names"
   " adding input info" },
  {"mincond",    'm', "NUM",      0,  "Minumum number of condition for "
   "rules. Default value: "
   STR(MINCONDDEFAULT)},
  {"maxcond",    'd', "NUM",      0,  "Maximum number of condition for "
   "rules generated. Default value is the number of the input "
   "attributes."},
  {"errormax",    'e', "PERC",      0,  "Maximum error accepted for "
   "the rules generated expressend in percentage. Default value: "
   STR(MAXERRDEFAULT)},
  {"mincovering",    'c', "PERC",      0,  "Minimum covering accepted "
   "for rules expressend in percentage. Default value: "
   STR(MINCOVDEFAULT)},
  {"startcovering",    'C', "PERC",      0,  "Starting covering threshold "
   "for rules expressend in percentage. Default value: "
   STR(STARTCOVDEFAULT)},
  {"testpercentage",    't', "PERC",      0,  "Percentage of the "
   "dataset to use for validation prupose. Default value: "
   STR(VALIDPERCDEFAULT)},
  {"output",   'o', "FILE", 0,
   "Output to FILE instead of standard output" },
  { 0 }
};
/* To communicate with ParseOpt. */
typedef struct {
  char *args[1];                /* arg1  */
  char autodim;
  int verbose, interactive, silent, ncondmax, ncondmin, longname;
  double startcovering, mincov, maxerrperc, validperc, probability;
  char *outfile;
} Arguments;
/* Parse a single option. */
static error_t ParseOpt (int key, char *arg, struct argp_state *state) {
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  Arguments *arguments = state->input;

  switch (key)
  {
  case 'p':
    arguments->probability = 1-atof(arg);
    break;
  case 'm':
    arguments->ncondmin = atoi(arg);
    break;
  case 'd':
    arguments->ncondmax = atoi(arg);
    break;
  case 'e':
    arguments->maxerrperc = atof(arg);
    break;
  case 't':
    arguments->validperc = atof(arg);
    break;
  case 'c':
    arguments->mincov = atof(arg);
    break;
  case 'C':
    arguments->startcovering = atof(arg);
    break;
  case 'i':
    arguments->interactive = 1;
    break;
 case 'l':
    arguments->longname = 1;
    break;
  case 'a':
    arguments->autodim = 1;
    break;
  case 'v':
    arguments->verbose = 1;
    break;
  case 'q':
    arguments->silent = 1;
    break;
  case 'o':
    arguments->outfile = arg;
    break;

  case ARGP_KEY_ARG:
    if (state->arg_num >= 2)
      /* Too many arguments. */
      argp_usage (state);

    arguments->args[state->arg_num] = arg;

    break;

  case ARGP_KEY_END:
    if (state->arg_num < 0)
      /* Not enough arguments. */
      argp_usage (state);
    break;

  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}
/* Our argp parser. */
static struct argp argp = { options, ParseOpt, args_doc, doc };


void InteractiveOtp(size_t *nextractmin, size_t *ncondmax, size_t *ncol,
                    char *autodim, double *mincov, double *maxerrperc,
                    double *validperc) {
  char *strtmp;

  printf("Insert the min number of condition ["
         STR(MINCONDDEFAULT)"]: ");
  strtmp = ReadLine();
  if(strcmp(strtmp, "\n"))
    *nextractmin = atol(strtmp);
  else
    *nextractmin = MINCONDDEFAULT;
  free(strtmp);

  printf("Insert the max number of condition [auto]: ");
  strtmp = ReadLine();
  if(strcmp(strtmp, "\n"))
    *ncondmax = atol(strtmp);
  else {
    *ncondmax = *ncol;
    *autodim = 1;
  }
  free(strtmp);

  printf("Insert the min covering in percentage ["
         STR(MINCOVDEFAULT)"]: ");
  strtmp = ReadLine();
  if(strcmp(strtmp, "\n"))
    *mincov = atof(strtmp);
  else
    *mincov = MINCOVDEFAULT;
  free(strtmp);

  printf("Insert the max error in percentage: ["
         STR(MAXERRDEFAULT)"]: ");
  strtmp = ReadLine();
  if(strcmp(strtmp, "\n"))
    *maxerrperc = atof(strtmp);
  else
    maxerrperc = MAXERRDEFAULT;
  free(strtmp);

  printf("Insert the percentage to use for validation: ["
         STR(VALIDPERCDEFAULT)"]: ");
  strtmp = ReadLine();
  if(strcmp(strtmp, "\n"))
    *validperc = atof(strtmp);
  else
    *validperc = VALIDPERCDEFAULT;
  free(strtmp);
}

int main(int argc, char **argv) {
  int32 nout, i;
  double cov, mincov, maxerrperc, validperc;
  size_t ncol, nelem, ncomb, j, nextract, ncondmax, tmpc, ncondmin, nseed,
    countother;
  clock_t begin, end;
  Arguments arguments;
  CharArray names, outnames;
  Rules *rules;
  size_t *noutclass, *stepcount, *errmaxv;
  Edges *limits;
  FILE *outfile;
  char **cindext, **cindaux;
  size_t **combs;
  KChunk ***chunks;
  char autodim = 0;
  size_t errtr = 0;
  size_t errval = 0;
  EdgesCollector *edcoll = NULL;


  arguments.probability = 1-RULEPROBDECT;
  arguments.interactive = 0;
  arguments.silent = 0;
  arguments.verbose = 0;
  arguments.outfile = "-";
  outfile = stdout;
  arguments.mincov = MINCOVDEFAULT;
  arguments.startcovering = STARTCOVDEFAULT;
  arguments.maxerrperc = MAXERRDEFAULT;
  arguments.validperc = VALIDPERCDEFAULT;
  arguments.ncondmin = MINCONDDEFAULT;
  arguments.ncondmax = INT32_MAX;
  arguments.autodim = 0;
  arguments.longname = 0;

  /* Parse our arguments; every option seen by ParseOpt will
     be reflected in arguments. */
  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  mincov = arguments.mincov;
  cov = arguments.startcovering;
  maxerrperc = arguments.maxerrperc;
  validperc = arguments.validperc;
  ncondmax = arguments.ncondmax;
  ncondmin = arguments.ncondmin;
  autodim = arguments.autodim;

  if (strcmp(arguments.outfile, "-"))
    outfile = fopen(arguments.outfile, "rb");
  if (!outfile)
    Error();

#ifdef OPENCL
  InitCl();
#endif
  if (argc==1)
    arguments.args[0] = "-";
  chunks = LoadChunks(arguments.args[0], &ncol, &nelem, &noutclass, &nout, &names, !arguments.longname,
                      &outnames, arguments.verbose);
  ncol--;
  limits = InitEdges(ncol);
  rules = InitRules(nout);

  if (arguments.interactive)
    InteractiveOtp(&ncondmin, &ncondmax, &ncol, &autodim, &mincov,
                   &maxerrperc, &validperc);
  if (ncondmax>ncol)
    ncondmax = ncol;

  stepcount = (size_t *)calloc(ncondmax, sizeof(size_t));
  errmaxv = (size_t *)alloca(nout*sizeof(size_t));
  SetTrValChunks(chunks, ncol, nout, validperc/100);

  FindLimits(chunks, limits, ncol, nout);

  for (i=0; i<nout; i++) {
    countother = 0;
    for (j=0; j<nout; j++)
      if (j!=i)
        countother += noutclass[i];
    errmaxv[i] = maxerrperc*countother/100;
  }


  /* for (i=0; i<nout; i++) */
  /*   errmaxv[i] = (size_t)(maxerrperc*(noutclass[i]/100)* */
  /*                         ((100-validperc)/100)); */

  edcoll = ResizeEdgesColletor(NULL, EDSTEP);

#ifdef OPENCL
  LoadChunksToDevice(chunks, ncol, nout);
#endif

  cindext = InitChunksInd(nout, chunks, -1);
  cindaux = InitChunksInd(nout, chunks, -1);

  begin = clock();

  while (cov>=mincov) {
    if (!arguments.silent)
      printf("Extracting rules with covering greater than %.2f%%\n", cov);
    nseed = EstimateSample(cov/100,
                           arguments.probability);
    SetChunksInd(cindext, nout, chunks, nseed);
    Ind1EqInd1ANDInd2(cindext, cindaux, nout, chunks);
    nseed = CountInd(cindext, nout, chunks);
    if(!nseed)
      break;
    if (arguments.verbose)
      printf("Number of seeds: %ld\n", CountInd(cindext, nout, chunks));

    for (nextract=ncondmin; nextract<=ncondmax; nextract++) {
      Combs(&combs, &ncomb, ncol, nextract);
      if (!arguments.silent)
        printf("Extracting rules with %ld conds\n", nextract);
      for (j=0; j<ncomb; j++) {
        if (!arguments.silent) {
          printf("Progress: %.2f%%\t", 100*(float)j/ncomb);
          printf("Explained pattern pergentage: %.2f%%\r", 100-100*
                 (float)CountInd(cindext, nout, chunks)/nseed);
          fflush(stdout);
        }
        tmpc = FindRules(limits, nout, ncol, combs[j], chunks, cov,
                         &edcoll, nextract, errmaxv, cindext);
        if (j==0)
          stepcount[nextract-1] = tmpc;
        else
          if (tmpc<stepcount[nextract-1])
            stepcount[nextract-1] = tmpc;
      }
      if (!arguments.silent) {
        printf("Progress: 100.00%%\t");
        printf("Explained pattern pergentage: %.2f%%\r", 100-100*
               (float)CountInd(cindext, nout, chunks)/nseed);
        fflush(stdout);
        printf("\n");
      }
      FreeCombs(combs, ncomb);
      if (autodim &&
          ((nextract>1 && (float)stepcount[nextract-1]/nelem<THRESHOLD) ||
           (nextract>2 && stepcount[nextract-2]<stepcount[nextract-1]))) {
        if (!arguments.silent)
          printf("Enough dim analysed going to sleep...\n");
        break;
      }
    }
    DropPointsAllRules(cindaux, nout, edcoll, chunks);
    cov/=2;
    /* cov--; */
    nseed = EstimateSample(cov/100,
                           arguments.probability);
  }
  end = clock();
  if (!arguments.silent)
    printf("Total time to build rules %f\n",
           (double)(end-begin)/CLOCKS_PER_SEC);

  for (i=0; i<edcoll->len; i++) {
    char *buf;
    Edges *ed;

    ed = edcoll->ed[i];
    buf = PrintRules(ed, ed->comb, &names);
    AppendRule(ed->nclass, ed->ndim, buf, &rules[ed->nclass]);
  }

  for (i=0; i<nout; i++) {
    if (!arguments.silent)
      fprintf(outfile, "Printing rules for output number: %d\n",
              i);
    for (j=0; j<rules[i].len; j++)
      fprintf(outfile, "%s", rules[i].v[j]);
  }

  ApplyRules(&errtr, &errval, nout, edcoll, ncol, chunks);
  validperc /= 100;
  nelem++;
  if (!arguments.silent) {
    printf("Correctly classified in trainingset : %.2f%% (%ld)\n",
           100*(1-(float)errtr/(nelem*(1-validperc))),
           (size_t)(nelem*(1-validperc))-errtr);
    if (validperc)
      printf("Correctly classified in validationset : %.2f%% (%ld)\n",
             100*(1-(float)errval/(nelem*validperc)),
             (size_t)(nelem*validperc)-errval);
  }
#ifdef OPENCL
  FreeChunksOnDevice(nout, nout);
#endif
  FreeChunksInd(cindaux, nout);
  FreeChunksInd(cindext, nout);
  FreeEdgesCollector(edcoll);
  FreeRules(rules, nout);
  FreeEdges(limits);
  FreeChunks(chunks, ncol, noutclass, nout);
  FreeCharArray(&names);
  /* FreeCharArray(&outnames); */
  free(noutclass);
  free(stepcount);

  return 0;
}
