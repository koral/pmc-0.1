/**************************************************************************
          pmcmem.c  -  Function for memory handling for PMC algorithm
          --------------------------------------------------------
    begin                : Sept 25 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "pmc.h"

float FindNEl(float *v, size_t ind) {
  size_t i = 0;
  size_t j = 0;

  if (!ind)
    return v[0];

  while (i!=ind) {
    if (v[j+1]!=v[j])
      i++;
    j++;
  }
  return v[j];
}

void CharReplace(char *str, char origc, char newc) {

  while(*(str++)!='\0')
    if (*str==origc)
      *str=newc;

  return;
}

int32 CmpFloat(const void *a, const void * b) {

  if((*(float *)a-*(float *)b)>0)
    return 1;
  else
    return 0;
}

int32 CmpInt32(const void *a, const void * b) {

  return (int32)(*(int32 *)a-*(int32 *)b);
}

size_t FindElChArray(CharArray *carray, char *str) {
  size_t i = 0, count = SIZE_MAX;

  while(i<carray->len) {
    if (!strcmp(str, carray->c[i++])) {
      count = i-1;
      break;
    }
  }
  if (count!=SIZE_MAX)
    return count;
  return SIZE_MAX;
}

void ResizeCharArray(CharArray *carray, size_t size) {
  carray->c = (char **)realloc((void *)carray->c, size*sizeof(char *));
  if (!carray->c)
    Error();

  carray->maxlen = size;
  memset(carray->c+carray->len, 0, carray->maxlen-carray->len);

  return;
}

void FreeCharArray(CharArray *carray) {
  size_t i;

  for (i=0; i<carray->len; i++)
    free(carray->c[i]);
  free(carray->c);
}

size_t AddStrToCharArray(char *str, CharArray *carray, char checkdup) {

  if (checkdup) {
    size_t ctmp = 0;

    ctmp = FindElChArray(carray, str);
    if (ctmp!=SIZE_MAX)
      return ctmp;
  }

  if (carray->len==carray->maxlen)
    ResizeCharArray(carray, carray->maxlen+STRINGSTEP);
  carray->c[carray->len] = (char *)malloc(MAXSTRING);
  strcpy(carray->c[carray->len], str);

  return carray->len++;
}

void InitCharArrray(CharArray *carray) {
  carray->len = 0;
  carray->maxlen = 0;
  carray->c = NULL;

  return;
}

Rules *InitRules(int32 nout) {
  int32 i;
  Rules *rules;

  rules = (Rules *)malloc(nout*sizeof(Rules));
  if (!rules)
    Error();
  for (i=0; i<nout; i++) {
    rules[i].maxlen = rules[i].len = 0;
    rules[i].nclass = NULL;
    rules[i].ncond = NULL;
    rules[i].cov = NULL;
    rules[i].err = NULL;
    rules[i].v = NULL;
    ResizeRules(&rules[i], STRINGSTEP);
  }
  return rules;
}
void FreeRules(Rules *rules, int32 nout) {
  int32 i;
  size_t j;

  for (i=0; i<nout; i++) {
    for (j=0; j<rules[i].len; j++)
      free(rules[i].v[j]);
    free(rules[i].v);
    free(rules[i].nclass);
    free(rules[i].ncond);
    free(rules[i].cov);
    free(rules[i].err);
  }
  free(rules);
}

void ResizeRules(Rules *rules, size_t size) {
  rules->v = (char **)realloc((void *)rules->v,
                              size*(rules->maxlen+sizeof(char *)));
  if (!rules->v)
    Error();

  rules->nclass = (int32 *)realloc((void *)rules->nclass,
                                   size*(rules->maxlen+sizeof(int32)));
  if (!rules->nclass)
    Error();

  rules->ncond = (size_t *)realloc((void *)rules->ncond,
                                   size*(rules->maxlen+sizeof(size_t)));
  if (!rules->ncond)
    Error();
  rules->cov = (double *)realloc((void *)rules->cov,
                                 size*(rules->maxlen+sizeof(double)));
  if (!rules->cov)
    Error();

  rules->err = (double *)realloc((void *)rules->err,
                                 size*(rules->maxlen+sizeof(double)));
  if (!rules->err)
    Error();

  rules->maxlen = size;

  return;
}

void AppendRule(int32 nclass, size_t ncond, char* str, Rules *rules) {

  if(rules->len==rules->maxlen)
    ResizeRules(rules, STRINGSTEP);
  rules->v[rules->len++] = str;

  return;
}

EdgesCollector *AppendEd(Edges *ed, int32 nclass, size_t nextract,
                         double cov, double err, size_t *comb, size_t ncol,
                         EdgesCollector *edcoll) {
  Edges *edtmp;

  if (edcoll->len==edcoll->maxlen)
    edcoll = ResizeEdgesColletor(edcoll, edcoll->maxlen+EDSTEP);
  edtmp = InitEdges(ncol);
  edtmp->ndim = nextract;
  edtmp->nclass = nclass;
  edtmp->cov = cov;
  edtmp->err = err;
  memcpy(edtmp->comb, comb, nextract*sizeof(size_t));
  memcpy(edtmp->max, ed->max, ncol*sizeof(int32));
  memcpy(edtmp->min, ed->min, ncol*sizeof(int32));
  edcoll->ed[edcoll->len++] = edtmp;

  return edcoll;
}

KChunk *ResizeChunk(KChunk *p, size_t size) {
  KChunk *ptmp = NULL;

  if (p) {
    if (posix_memalign((void **)&ptmp, ALIGNSIZE,
                       sizeof(KChunk)+(size*sizeof(int32)-1)))
      Error();
    memcpy(ptmp, p, p->len);
    free(p);
    p = ptmp;
  }
  else
    if (posix_memalign((void **)&p, ALIGNSIZE,
                       sizeof(KChunk)+(size*sizeof(int32)-1)))
      Error();

  p->maxlen = size;
  return p;
}

EdgesCollector *ResizeEdgesColletor(EdgesCollector *p, size_t size) {
  char tmp = 0;

  if (!p)
    tmp = 1;
  p = realloc(p,
              size*sizeof(Edges)+(sizeof(EdgesCollector)-sizeof(Edges)));
  p->maxlen = size;
  if (tmp)
    p->len = 0;
  return p;
}

void FreeEdgesCollector(EdgesCollector *p) {
  size_t i;

  for (i=0; i<p->len; i++)
    FreeEdges(p->ed[i]);
  free(p);
}

size_t FindIndInt64(size_t *v, size_t el, size_t maxlen) {
  size_t i;

  for (i=0; i<maxlen; i++) {
    if (v[i]==el)
      return i;
  }
  return -1;
}

size_t FindIndInt32(int32 *v, int32 el, size_t maxlen) {
  size_t i;

  for (i=0; i<maxlen; i++) {
    if (v[i]==el)
      return i;
  }
  return -1;
}

size_t FindIndFloat(float *v, float el, size_t maxlen) {
  size_t i;

  for (i=0; i<maxlen; i++)
    if (v[i]==el)
      return i;
  return -1;
}

char *FindNewEl(char *filebuf, char sep) {
  size_t i;

  for (i=0; filebuf[i+1]!='\0'; i++)
    if (filebuf[i]==sep)
      return filebuf+i+1;
  return NULL;
}

char **InitChunksInd(int32 nout, KChunk ***chunks, size_t nsample) {
  int32 i;
  char **ind;

  ind = (char **)malloc(nout*sizeof(char *));
  if (!ind)
    Error();
  for (i=0; i<nout; i++)
    if (posix_memalign((void **)&ind[i], ALIGNSIZE,
                       chunks[i][0]->trainlen*sizeof(char)))
      Error();
  SetChunksInd(ind, nout, chunks, nsample);

  return ind;
}

void SetChunksInd(char **ind, int32 nout, KChunk ***chunks,
                  size_t nsample) {
  int32 i, seedfract;
  size_t j;

  for (i=0; i<nout; i++) {
    if (nsample==-1)
      memset((void *)ind[i], 1, chunks[i][0]->trainlen*sizeof(char));
    else {
      seedfract = chunks[i][0]->trainlen/nsample;
      if (!seedfract)
        seedfract = 1;
      memset((void *)ind[i], 0, chunks[i][0]->trainlen*sizeof(char));
      for (j=0; j<chunks[i][0]->trainlen/seedfract; j++) {
        ind[i][j*seedfract] = 1;
      }
    }
  }
  return;
}

void FreeChunksInd(char **cind, int32 nout) {
  int32 i;

  for (i=0; i<nout; i++)
    free(cind[i]);
  free(cind);
}

Edges *InitEdges(size_t ncol) {
  Edges *ed;

  ed = (Edges *)malloc(sizeof(Edges));
  if (!ed)
    Error();
  ed->ndim = ncol;
  if (posix_memalign((void **)&ed->type, ALIGNSIZE, ncol*sizeof(InpType)))
    Error();
  memset(ed->type, INT, ncol*sizeof(*ed->type));
  if (posix_memalign((void **)&ed->comb, ALIGNSIZE, ncol*sizeof(size_t)))
    Error();
  if (posix_memalign((void **)&ed->max, ALIGNSIZE, ncol*sizeof(int32)))
    Error();
  if (posix_memalign((void **)&ed->min, ALIGNSIZE, ncol*sizeof(int32)))
    Error();

  return ed;
}

void FreeEdges(Edges *ed) {
  free(ed->max);
  free(ed->min);
  free(ed->comb);
  free(ed->type);
  free(ed);
}

void InitVoidChunks(KChunk ****chunks_ref, size_t ncol, int32 nout) {
  size_t i;
  KChunk ***chunks;

  chunks = (KChunk ***)malloc((nout)*sizeof(KChunk ***));
  for (i=0; i<nout; i++)
    chunks[i] = (KChunk **)calloc(1, (ncol+nout+1)*sizeof(KChunk **));
  *chunks_ref = chunks;
}

void InitChunks(KChunk ****chunks_ref, size_t ncol, size_t nelem,
                size_t *noutclass, int32 nout, char *types) {
  size_t i, j;
  KChunk ***chunks;

  InitVoidChunks(&chunks, ncol, nout);
  for (i=0; i<nout; i++) {
    for (j=0; j<ncol+nout+1; j++) {
      chunks[i][j] = NULL;
      chunks[i][j] = ResizeChunk(chunks[i][j], noutclass[i]);
      if (j<ncol)
        if (types[j]=='i')
          chunks[i][j]->type = INT;
        else
          chunks[i][j]->type = FLOAT;
      else
        chunks[i][j]->type = INT;
      chunks[i][j]->len = 0;
    }
  }
  *chunks_ref = chunks;
}

void SetTrValChunks(KChunk ***chunks, size_t ncol, int32 nout,
                    double validperc) {
  size_t i, j;

  for (i=0; i<nout; i++)
    for (j=0; j<ncol+nout; j++)
      //if (chunks[i][j]->len>1)
      chunks[i][j]->trainlen = chunks[i][j]->len*(1-validperc);
  /* else  //when you have one case of that class */
  /*   chunks[i][j]->trainlen = chunks[i][j]->len; */
}


void FreeCombs(size_t **combs, size_t ncomb) {
  size_t i;

  for (i=0; i<ncomb; i++)
    free(combs[i]);
  free(combs);
}

void FreeChunks(KChunk ***chunks, size_t ncol, size_t *noutclass,
                int32 nout) {
  size_t i, j;

  for (i=0; i<nout; i++)
    for (j=0; j<ncol+nout+1; j++)
      free(chunks[i][j]);

  for (i=0; i<nout; i++)
    free(chunks[i]);

  free(chunks);
}

void Error() {
  printf("sorry something bad append :\\ \n");
  exit(1);
}
