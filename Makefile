
#       Makefile  -  Main Makefile for PMC algorithm
#       --------------------------------------------------------
# begin                : Sept 25 2013
# copyright            : (C) 2013 by Andrea Corallo
# email                : andrea_corallo@yahoo.it

# This file is part of PMC.

# PMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# PMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with PMC.  If not, see <http://www.gnu.org/licenses/>.


#./pmc-disc -n -s'\t' datasets/set.set > datasets/set.int
#./pmc -q datasets/set.int > rules.pmc
#./pmc-crule -n -i rules.pmc -s'\t' datasets/set.set

HEADERS = pmc.h
OBJ = pmc.o pmccore.o pmcmem.o pmcfile.o
HDOBJ = pmccore.o pmcmem.o pmcfile.o pmcmapred.o
MRTESTOBJ = pmc-mrtest.o pmccore.o pmcmem.o pmcfile.o pmcmapred.c
DISCOBJ = pmc-disc.o pmccore.o pmcmem.o pmcfile.o
CFLAGS = -g -Wall
CC = gcc
CCPP = g++
VALGRINDOPT = --leak-check=full #--show-reachable=yes
HDINCLUDE = -I/home/andrea/hadoop-2.2.0-src/hadoop-tools/hadoop-pipes/src/main/native/utils/api -I/home/andrea/hadoop-2.2.0-src/hadoop-tools/hadoop-pipes/src/main/native/pipes/api -I/home/andrea/hadoop-2.2.0-src/hadoop-tools/hadoop-pipes/src

ifeq ($(HADOOP),True)
HDMACROS = -D_REENTRANT -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -DHADOOP
else
HDMACROS =
endif

ifeq ($(PROFILE),True)
PROFFLAG = -pg
else
PROFFLAG =
endif

ifeq ($(MODE),Release)
CFLAGS = -O3 -Wall
endif

ifeq ($(CL),True)
#CLFLAGS = -L/usr/lib/nvidia-304/ -I/usr/include/CL/
#CLFLAGS = -L/usr/lib64/ -I/opt/nvidia/cuda/include/ #(amazon ec2)
CLFLAGS = -L/usr/lib64/ -I/opt/AMDAPP/include/
CLLINK = -lOpenCL
MACROS = -DOPENCL
OBJ = pmc.o pmccore.o pmcmem.o pmcfile.o pmccl.o cl-helper.o
PMCNAME = pmccl
else
CLFLAGS =
CLLINK =
MACROS =
PMCNAME = pmc
endif

default: pmc pmc-preparehdfiles pmc-mrtest pmc-disc pmc-crule

%.o: %.c $(HEADERS)
	$(CC) $(CLFLAGS) $(HDMACROS) -c -o $@ $< $(CFLAGS) $(MACROS) $(CLLINK) $(PROFFLAG)

%.o: %.cpp $(HEADERS)
	$(CCPP) $(CLFLAGS) $(HDMACROS) $(HDINCLUDE) -c -o $@ $< $(CFLAGS) $(MACROS) $(CLLINK) $(PROFFLAG)

pmc: $(OBJ)
	$(CC) $(CLFLAGS) $(CFLAGS) $(OBJ) -o $(PMCNAME) $(MACROS) $(CLLINK) $(PROFFLAG) -lm
	# cp $(PMCNAME) bin/

pmc-preparehdfiles: $(HDOBJ) pmc-preparehdfiles.o
	$(CC) $(CLFLAGS) $(CFLAGS) $(HDOBJ) pmc-preparehdfiles.o -o pmc-preparehdfiles $(MACROS) $(CLLINK) $(PROFFLAG) -lm

pmc-mrtest: $(MRTESTOBJ)
	$(CC) $(CLFLAGS) $(CFLAGS) $(MRTESTOBJ) -o pmc-mrtest $(MACROS) $(CLLINK) $(PROFFLAG) -lm

pmc-hadoop: $(HDOBJ) pmc-hadoop.o
	$(CCPP) $(CFLAGS) $(HDMACROS) $(HDOBJ) pmc-hadoop.o -o pmc-hadoop -rdynamic libhadooppipes.a libhadooputils.a -lssl -lcrypto -lpthread -lm

pmc-hadoop-run: pmc-hadoop
	./launch-pmc-hadoop.sh

pmc-disc: $(DISCOBJ)
	$(CC) $(CLFLAGS) $(CFLAGS) $(DISCOBJ) -o pmc-disc $(MACROS) $(CLLINK) $(PROFFLAG) -lm

pmc-test: pmcparser_gen pmc-test.o pmccore.o pmcmem.o pmcfile.o pmcparser.o pmclexer.o
	$(CC) $(CLFLAGS) $(CFLAGS) pmc-test.o pmccore.o pmcmem.o pmcfile.o pmcparser.o pmclexer.o -o pmc-test $(MACROS) $(CLLINK) $(PROFFLAG)

pmc-crule: pmcparser_gen pmc-crule.o pmccore.o pmcmem.o pmcfile.o pmcparser.o pmclexer.o
	$(CC) $(CLFLAGS) $(CFLAGS) pmc-crule.o pmccore.o pmcmem.o pmcfile.o pmcparser.o pmclexer.o -o pmc-crule $(MACROS) $(CLLINK) $(PROFFLAG) -lm

pmcparser_gen: pmcparser.y pmclexer.l
	bison -v -y -d pmcparser.y -o pmcparser.c
	flex -o pmclexer.c pmclexer.l

clean: cleantmp
	-rm -f *.o
	-rm -f pmc
	-rm -f pmccl
	-rm -f pmc-preparehdfiles
	-rm -f pmc-mrtest
	-rm -f pmc-hadoop
	-rm -f pmc-disc
	-rm -f pmc-test
	-rm -f pmc-crule
	-rm -f *.orig
	-rm -f *.out
	-rm -f callgrind*
	-rm gmon.*
cleantmp:
	-rm tmp/*
tags:
	etags -R *

valgrind: pmc
	valgrind --log-file=valsum.out $(VALGRINDOPT) ./pmc datasets/file1000p_1000v_10dim_0err.int

valgrind-mrtest: pmc-mrtest
	valgrind --log-file=valsum.out $(VALGRINDOPT) ./pmc-mrtest

valgrind-disc: pmc-disc
	valgrind --log-file=valsum.out $(VALGRINDOPT) ./pmc-disc  -n -s'\t' datasets/disc.set

valgrind-crule: pmc-crule
	valgrind --log-file=valsum.out $(VALGRINDOPT) ./pmc-crule -n -s'\t' -irules.pmc datasets/set.set

callgrind: pmc
	valgrind --tool=callgrind ./pmc file100p_10dim.set && kcachegrind callgrind*

countlines:
	ls *.y *.l *.c *.cpp *.sh *.h *.cl Makefile | tr ' ' '\n' | xargs wc -l
profile:
	gprof pmc gmon.out > gmon.txt