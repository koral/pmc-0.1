/**************************************************************************
          pmcdisc.c  -  Interface to translate a dataset in discrete values
          --------------------------------------------------------
    begin                : Nov 21 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "pmc.h"

FILE *outfile;
const char *argp_program_version =
  "pmc-disc 0.1";
const char *argp_program_bug_address =
  "<andrea_corallo@yahoo.it>";
/* Program documentation. */
static char doc[] =
  "pmc-disc Prepare the dataset to be used by pmc converting all the values"
  " to int";
/* Description of the arguments we accept. */
static char args_doc[] = "FILE";
/* Options we understand. */
static struct argp_option options[] = {
  {"verbose",  'v', 0,      0,  "Produce verbose output" },
  {"names",  'n', 0,      0,  "Takes input names from line 2" },
  {"separator",'s', "'SEPARATOR_CHAR'",      0,
   "Specify the separator character. Default is comma"},
  {"missing",  'm', "'MISSING_STRING'",      0,
   "Specify the missing string. Default is ?"},
  {"intdiscretize",  'i',       0,      0,
   "Discretize also int inputs"},
  {"output",   'o', "FILE", 0,
   "Output to FILE instead of standard output" },
  { 0 }
};
/* To communicate with ParseOpt. */
typedef struct Arguments
{
  char *args[1];                /* arg1  */
  int verbose, silent, discint, names;
  char *sep, *miss, *outfile;
}arguments;
/* Parse a single option. */
static error_t ParseOpt (int key, char *arg, struct argp_state *state) {
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  struct Arguments *arguments = state->input;

  switch (key)
  {
  case 's':
    arguments->sep = arg;
    break;
  case 'm':
    arguments->miss = arg;
    break;
  case 'v':
    arguments->verbose = 1;
    break;
  case 'n':
    arguments->names = 1;
    break;
  case 'i':
    arguments->discint = 1;
    break;
  case 'o':
    arguments->outfile = arg;
    break;

  case ARGP_KEY_ARG:
    if (state->arg_num >= 1)
      /* Too many arguments. */
      argp_usage (state);
    arguments->args[state->arg_num] = arg;

    break;

  case ARGP_KEY_END:
    if (state->arg_num < 1)
      /* Not enough arguments. */
      argp_usage (state);
    break;

  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}
/* argp parser. */
static struct argp argp = { options, ParseOpt, args_doc, doc };

int main(int argc, char **argv) {
  char sepc;
  size_t nline, ncol, i, j;
  arguments arguments;
  char *type;
  Elem32 *elemv;
  int32 *intv;
  size_t *ind;
  CharArray carray, names;
  Elem32 **col;

  /* Default values */
  arguments.names = 0;
  arguments.verbose = 0;
  arguments.discint = 0;
  arguments.outfile = "-";
  arguments.miss = "?";
  arguments.sep = ",";
  outfile = stdout;

  /* Parse our arguments; every option seen by ParseOpt will
     be reflected in arguments. */
  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  if (!strcmp(arguments.sep, "\\t"))
    arguments.sep = "\t";
  sepc = arguments.sep[0];

  if (strcmp(arguments.outfile, "-"))
    outfile = fopen(arguments.outfile, "rb");
  if (!outfile)
    Error();

  InitCharArrray(&carray);

  LoadHeterogeneousFile(arguments.args[0], &ncol, &type, sepc, &ind,
                        &nline, &names, arguments.names, arguments.discint,
                        arguments.miss, &col, &carray);


  nline = nline-arguments.names-1;
  elemv = (Elem32 *)malloc((nline)*sizeof(Elem32));
  intv = (int *)malloc((nline)*sizeof(int));

  for (j=0; j<ncol; j++) {
    if (arguments.verbose)
      printf("Working on col number %ld\n", j);
    if (arguments.discint && type[j]=='i') {
      memcpy(elemv, col[ind[j]], nline*sizeof(Elem32));
      qsort(elemv, nline, sizeof(Elem32), CmpInt32);
      intv[0] = 0;
      for (i=1; i<nline; i++)
        if (elemv[i].i>elemv[i-1].i)
          intv[i] = intv[i-1]+1;
        else
          intv[i] = intv[i-1];
      for (i=0; i<nline; i++) {
        if (col[ind[j]][i].i!=MISSVAL)
          col[ind[j]][i].i =
            intv[FindIndInt32((int32 *)elemv, col[ind[j]][i].i, nline)];
      }
    }
    else if (type[j]=='f') {
      memcpy(elemv, col[ind[j]], nline*sizeof(Elem32));
      qsort(elemv, nline, sizeof(Elem32), CmpFloat);
      intv[0] = 0;
      for (i=1; i<nline; i++)
        if (elemv[i].f-elemv[i-1].f>0)
          intv[i] = intv[i-1]+1;
        else
          intv[i] = intv[i-1];
      for (i=0; i<nline; i++) {
        if (col[ind[j]][i].i!=MISSVAL)
          col[ind[j]][i].i =
              intv[FindIndFloat((float *)elemv, col[ind[j]][i].f, nline)];
      }
    }
  }

  for (i=0; i<ncol-1; i++)
    fprintf(outfile, "i\t");
  fprintf(outfile, "o");
  fprintf(outfile, "\n");
  for (i=0; i<ncol; i++)
    fprintf(outfile, "%s\t", names.c[i]);
  fprintf(outfile, "\n");
  for (i=0; i<nline; i++) {
    for (j=0; j<ncol-1; j++)
      fprintf(outfile, "%d\t", col[j][i].i);
    fprintf(outfile, "%d\n", col[j][i].i);
  }

  if (outfile!=stdout)
    fclose(outfile);
  for (i=0; i<ncol; i++)
    free(col[i]);
  FreeCharArray(&carray);
  FreeCharArray(&names);
  free(col);
  free(type);
  free(ind);
  free(elemv);
  free(intv);

  return 0;
}
