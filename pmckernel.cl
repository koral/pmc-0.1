/**************************************************************************
          pmccl.c  -   Kernel function for PMC algorithm in OpenCL
          --------------------------------------------------------
    begin                : Oct 4 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#define MISSVAL -2147483648

typedef int int32;
typedef unsigned int uint32;
typedef long int int64;

__kernel void ReduceTwoStep(__global char* clvres,
                            __local char* scratch,
                            __global uint32* cllen,
                            __global uint32* clvsum) {

  uint32 gid = get_global_id(0);
  uint32 accumulator = 0;
// Loop sequentially over chunks of input vector

  while (gid<*cllen) {
/* char element = clvres[gid]; */
    accumulator = accumulator+clvres[gid];
    gid += get_global_size(0);
  }

// Perform parallel reduction
  uint32 lid = get_local_id(0);
  scratch[lid] = accumulator;
  barrier(CLK_LOCAL_MEM_FENCE);
  for(int offset = get_local_size(0)/2;
      offset>0;
      offset=offset/2) {
    if (lid<offset) {
/* char other = scratch[lid+offset]; */
/* char mine = scratch[lid]; */
      scratch[lid] = scratch[lid]+scratch[lid+offset];
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  if (lid==0)
    clvsum[get_group_id(0)] = scratch[0];
}

__kernel void VChunk1(__global int32 *clchunk1,
                      __global char *clvres,
                      __constant  int32 *cled,
                      __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    clvres[gid] =
      (clchunk1[gid]==MISSVAL ||
       (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[1]));

  return;
}

__kernel void VChunk2(__global int32 *clchunk1,
                      __global int32 *clchunk2,
                      __global char *clvres,
                      __constant int32 *cled,
                      __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    clvres[gid] =
      (clchunk1[gid]==MISSVAL ||
       (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[2])) &&
      (clchunk2[gid]==MISSVAL ||
       (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[3]));

  return;
}

__kernel void VChunk3(__global int32 *clchunk1,
                      __global int32 *clchunk2,
                      __global int32 *clchunk3,
                      __global char *clvres,
                      __constant int32 *cled,
                      __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    clvres[gid] =
      (clchunk1[gid]==MISSVAL ||
       (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[3])) &&
      (clchunk2[gid]==MISSVAL ||
       (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[4])) &&
      (clchunk3[gid]==MISSVAL ||
       (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[5]));

  return;
}

__kernel void VChunk4(__global int32 *clchunk1,
                      __global int32 *clchunk2,
                      __global int32 *clchunk3,
                      __global int32 *clchunk4,
                      __global char *clvres,
                      __constant int32 *cled,
                      __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    clvres[gid] =
      (clchunk1[gid]==MISSVAL ||
       (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[4])) &&
      (clchunk2[gid]==MISSVAL ||
       (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[5])) &&
      (clchunk3[gid]==MISSVAL ||
       (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[6])) &&
      (clchunk4[gid]==MISSVAL ||
       (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[7]));

  return;
}

__kernel void VChunk5(__global int32 *clchunk1,
                      __global int32 *clchunk2,
                      __global int32 *clchunk3,
                      __global int32 *clchunk4,
                      __global int32 *clchunk5,
                      __global char *clvres,
                      __constant int32 *cled,
                      __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    clvres[gid] =
      (clchunk1[gid]==MISSVAL ||
       (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[5])) &&
      (clchunk2[gid]==MISSVAL ||
       (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[6])) &&
      (clchunk3[gid]==MISSVAL ||
       (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[7])) &&
      (clchunk4[gid]==MISSVAL ||
       (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[8])) &&
      (clchunk5[gid]==MISSVAL ||
       (clchunk5[gid]>=cled[4] && clchunk5[gid]<=cled[9]));

  return;
}

__kernel void VChunk6(__global int32 *clchunk1,
                      __global int32 *clchunk2,
                      __global int32 *clchunk3,
                      __global int32 *clchunk4,
                      __global int32 *clchunk5,
                      __global int32 *clchunk6,
                      __global char *clvres,
                      __constant int32 *cled,
                      __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    clvres[gid] =
      (clchunk1[gid]==MISSVAL ||
       (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[6])) &&
      (clchunk2[gid]==MISSVAL ||
       (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[7])) &&
      (clchunk3[gid]==MISSVAL ||
       (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[8])) &&
      (clchunk4[gid]==MISSVAL ||
       (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[9])) &&
      (clchunk5[gid]==MISSVAL ||
       (clchunk5[gid]>=cled[4] && clchunk5[gid]<=cled[10])) &&
      (clchunk6[gid]==MISSVAL ||
       (clchunk6[gid]>=cled[5] && clchunk6[gid]<=cled[11]));

  return;
}

__kernel void VChunk7(__global int32 *clchunk1,
                      __global int32 *clchunk2,
                      __global int32 *clchunk3,
                      __global int32 *clchunk4,
                      __global int32 *clchunk5,
                      __global int32 *clchunk6,
                      __global int32 *clchunk7,
                      __global char *clvres,
                      __constant int32 *cled,
                      __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    clvres[gid] =
      (clchunk1[gid]==MISSVAL ||
       (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[7])) &&
      (clchunk2[gid]==MISSVAL ||
       (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[8])) &&
      (clchunk3[gid]==MISSVAL ||
       (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[9])) &&
      (clchunk4[gid]==MISSVAL ||
       (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[10])) &&
      (clchunk5[gid]==MISSVAL ||
       (clchunk5[gid]>=cled[4] && clchunk5[gid]<=cled[11])) &&
      (clchunk6[gid]==MISSVAL ||
       (clchunk6[gid]>=cled[5] && clchunk6[gid]<=cled[12])) &&
      (clchunk7[gid]==MISSVAL ||
       (clchunk7[gid]>=cled[6] && clchunk7[gid]<=cled[13]));

  return;
}

__kernel void VChunk8(__global int32 *clchunk1,
                      __global int32 *clchunk2,
                      __global int32 *clchunk3,
                      __global int32 *clchunk4,
                      __global int32 *clchunk5,
                      __global int32 *clchunk6,
                      __global int32 *clchunk7,
                      __global int32 *clchunk8,
                      __global char *clvres,
                      __constant int32 *cled,
                      __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    clvres[gid] =
      (clchunk1[gid]==MISSVAL ||
       (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[8])) &&
      (clchunk2[gid]==MISSVAL ||
       (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[9])) &&
      (clchunk3[gid]==MISSVAL ||
       (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[10])) &&
      (clchunk4[gid]==MISSVAL ||
       (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[11])) &&
      (clchunk5[gid]==MISSVAL ||
       (clchunk5[gid]>=cled[4] && clchunk5[gid]<=cled[12])) &&
      (clchunk6[gid]==MISSVAL ||
       (clchunk6[gid]>=cled[5] && clchunk6[gid]<=cled[13])) &&
      (clchunk7[gid]==MISSVAL ||
       (clchunk7[gid]>=cled[6] && clchunk7[gid]<=cled[14])) &&
      (clchunk8[gid]==MISSVAL ||
       (clchunk8[gid]>=cled[7] && clchunk8[gid]<=cled[15]));

  return;
}

__kernel void VChunkAdd1(__global int32 *clchunk1,
                         __global char *clvres,
                         __constant  int32 *cled,
                         __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    if (clvres[gid])
      clvres[gid] =
        !(clchunk1[gid]==MISSVAL ||
          (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[1]));

  return;
}

__kernel void VChunkAdd2(__global int32 *clchunk1,
                         __global int32 *clchunk2,
                         __global char *clvres,
                         __constant int32 *cled,
                         __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    if (clvres[gid])
      clvres[gid] =
        !((clchunk1[gid]==MISSVAL ||
           (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[2])) &&
          (clchunk2[gid]==MISSVAL ||
           (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[3])));

  return;
}

__kernel void VChunkAdd3(__global int32 *clchunk1,
                         __global int32 *clchunk2,
                         __global int32 *clchunk3,
                         __global char *clvres,
                         __constant int32 *cled,
                         __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    if (clvres[gid])
      clvres[gid] =
        !((clchunk1[gid]==MISSVAL ||
           (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[3])) &&
          (clchunk2[gid]==MISSVAL ||
           (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[4])) &&
          (clchunk3[gid]==MISSVAL ||
           (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[5])));

  return;
}

__kernel void VChunkAdd4(__global int32 *clchunk1,
                         __global int32 *clchunk2,
                         __global int32 *clchunk3,
                         __global int32 *clchunk4,
                         __global char *clvres,
                         __constant int32 *cled,
                         __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    if (clvres[gid])
      clvres[gid] =
        !((clchunk1[gid]==MISSVAL ||
           (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[4])) &&
          (clchunk2[gid]==MISSVAL ||
           (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[5])) &&
          (clchunk3[gid]==MISSVAL ||
           (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[6])) &&
          (clchunk4[gid]==MISSVAL ||
           (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[7])));

  return;
}

__kernel void VChunkAdd5(__global int32 *clchunk1,
                         __global int32 *clchunk2,
                         __global int32 *clchunk3,
                         __global int32 *clchunk4,
                         __global int32 *clchunk5,
                         __global char *clvres,
                         __constant int32 *cled,
                         __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    if (clvres[gid])
      clvres[gid] =
        !((clchunk1[gid]==MISSVAL ||
           (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[5])) &&
          (clchunk2[gid]==MISSVAL ||
           (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[6])) &&
          (clchunk3[gid]==MISSVAL ||
           (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[7])) &&
          (clchunk4[gid]==MISSVAL ||
           (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[8])) &&
          (clchunk5[gid]==MISSVAL ||
           (clchunk5[gid]>=cled[4] && clchunk5[gid]<=cled[9])));

  return;
}

__kernel void VChunkAdd6(__global int32 *clchunk1,
                         __global int32 *clchunk2,
                         __global int32 *clchunk3,
                         __global int32 *clchunk4,
                         __global int32 *clchunk5,
                         __global int32 *clchunk6,
                         __global char *clvres,
                         __constant int32 *cled,
                         __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    if (clvres[gid])
      clvres[gid] =
        !((clchunk1[gid]==MISSVAL ||
           (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[6])) &&
          (clchunk2[gid]==MISSVAL ||
           (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[7])) &&
          (clchunk3[gid]==MISSVAL ||
           (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[8])) &&
          (clchunk4[gid]==MISSVAL ||
           (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[9])) &&
          (clchunk5[gid]==MISSVAL ||
           (clchunk5[gid]>=cled[4] && clchunk5[gid]<=cled[10])) &&
          (clchunk6[gid]==MISSVAL ||
           (clchunk6[gid]>=cled[5] && clchunk6[gid]<=cled[11])));

  return;
}

__kernel void VChunkAdd7(__global int32 *clchunk1,
                         __global int32 *clchunk2,
                         __global int32 *clchunk3,
                         __global int32 *clchunk4,
                         __global int32 *clchunk5,
                         __global int32 *clchunk6,
                         __global int32 *clchunk7,
                         __global char *clvres,
                         __constant int32 *cled,
                         __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    if (clvres[gid])
      clvres[gid] =
        !((clchunk1[gid]==MISSVAL ||
           (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[7])) &&
          (clchunk2[gid]==MISSVAL ||
           (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[8])) &&
          (clchunk3[gid]==MISSVAL ||
           (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[9])) &&
          (clchunk4[gid]==MISSVAL ||
           (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[10])) &&
          (clchunk5[gid]==MISSVAL ||
           (clchunk5[gid]>=cled[4] && clchunk5[gid]<=cled[11])) &&
          (clchunk6[gid]==MISSVAL ||
           (clchunk6[gid]>=cled[5] && clchunk6[gid]<=cled[12])) &&
          (clchunk7[gid]==MISSVAL ||
           (clchunk7[gid]>=cled[6] && clchunk7[gid]<=cled[13])));

  return;
}

__kernel void VChunkAdd8(__global int32 *clchunk1,
                         __global int32 *clchunk2,
                         __global int32 *clchunk3,
                         __global int32 *clchunk4,
                         __global int32 *clchunk5,
                         __global int32 *clchunk6,
                         __global int32 *clchunk7,
                         __global int32 *clchunk8,
                         __global char *clvres,
                         __constant int32 *cled,
                         __constant uint32 *cllen)
{
  size_t gid = get_global_id(0);

  if (gid<*cllen)
    if (clvres[gid])
      clvres[gid] =
        !((clchunk1[gid]==MISSVAL ||
           (clchunk1[gid]>=cled[0] && clchunk1[gid]<=cled[8])) &&
          (clchunk2[gid]==MISSVAL ||
           (clchunk2[gid]>=cled[1] && clchunk2[gid]<=cled[9])) &&
          (clchunk3[gid]==MISSVAL ||
           (clchunk3[gid]>=cled[2] && clchunk3[gid]<=cled[10])) &&
          (clchunk4[gid]==MISSVAL ||
           (clchunk4[gid]>=cled[3] && clchunk4[gid]<=cled[11])) &&
          (clchunk5[gid]==MISSVAL ||
           (clchunk5[gid]>=cled[4] && clchunk5[gid]<=cled[12])) &&
          (clchunk6[gid]==MISSVAL ||
           (clchunk6[gid]>=cled[5] && clchunk6[gid]<=cled[13])) &&
          (clchunk7[gid]==MISSVAL ||
           (clchunk7[gid]>=cled[6] && clchunk7[gid]<=cled[14])) &&
          (clchunk8[gid]==MISSVAL ||
           (clchunk8[gid]>=cled[7] && clchunk8[gid]<=cled[15])));

  return;
}
