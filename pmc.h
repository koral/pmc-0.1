/**************************************************************************
          pmc.h  -  Header file for PMC algorithm
          --------------------------------------------------------
    begin                : Sept 25 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include <stdint.h>
#include <float.h>
#include <stdlib.h>
#include <alloca.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <argp.h>
#include <math.h>

/**************************************************************************

      PMC definitions

**************************************************************************/

#define DEF2STR(x) #x
#define STR(x) DEF2STR(x)
#ifndef HADOOP
#define TEMPFILEDIR tmp/
#else
#define TEMPFILEDIR ./
#endif
#define CHUNKFILENAME chunk-%d-%ld.bin
#define CHUNKINDFILENAME chunkind-%d.bin
#define HDINPUTFILENAME input.txt
#define INPUTDEFAULTNAME Inp%ld
#define RULESFILENAME1 rules
#define RULESFILENAME2 -%ld
#define RULESFILENAME3 .log
#define MINCONDDEFAULT 1
#define VALIDPERCDEFAULT 0
#define STARTCOVDEFAULT 10
#define MINCOVDEFAULT 5
#define MAXERRDEFAULT 0
#define STRINGSTEP (1 << 10)
#define EDSTEP (1 << 10)
#define MAXCLASSOUT (1 << 9)
#define ALIGNSIZE (1 << 6)
#define FRACTSTEP 1 //(1 << 2)
#define MAXSTRING (1 << 10)
#define THRESHOLD 0.1
#define MAXCONDCL 8
#define MAXCONDTEST 64
#define WAVEFRONT 64
#define WAVEFRONTREDUCE 256
#define CLFASTTHRESHOLD 100000
#define RULEPROBDECT 0.99
#define MISSVAL INT32_MIN
#define MAXVAL INT32_MAX
#define MINVAL (INT32_MIN+1)
#define MINFVAL FLT_MIN
#define MAXFVAL FLT_MAX

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef enum {MIN, MAX, UNKNOWN} MinMax;

typedef enum {INT, FLOAT} InpType;

typedef union Elem32 {
  int32 i;
  float f;
} Elem32;

typedef struct {
  size_t len;
  size_t trainlen;
  size_t maxlen;
  InpType type;
  Elem32 v[1];
} KChunk;

typedef struct {
  size_t len;
  size_t maxlen;
  int32 *nclass;
  size_t *ncond;
  double *cov;
  double *err;
  char **v;
} Rules;

typedef struct {
  size_t ndim;
  double cov;
  double err;
  InpType *type;
  size_t *comb;
  Elem32 *max;
  Elem32 *min;
  int32 nclass;
} Edges;

typedef struct {
  size_t len;
  size_t maxlen;
  Edges *ed[1];
} EdgesCollector;

typedef struct {
  size_t len;
  size_t maxlen;
  char **c;
} CharArray;

typedef struct {
  size_t inp;
  int32 min;
  int32 max;
} Cond;

typedef struct parse_parm_s
{
  void *yyscanner;     /* state of the lexer */
  char *buf;           /* buffer we read from */
} ParseParam;



/**************************************************************************

      Section with externing OpneCl env variable

**************************************************************************/

#ifdef OPENCL
#include "timing.h"
#include "cl-helper.h"
extern char growing, fastcl;
extern cl_context ctx;
extern cl_command_queue queue;
extern cl_device_id device;
extern long long devtype;
extern cl_uint computeunits;
extern cl_mem cled, cllen, clvres, clvsum, clvind;
extern cl_kernel knl_ReduceTwoStep;
extern size_t oldlen;
extern size_t ldim[1];
extern size_t gdim[1];
extern cl_kernel *knl_VChunk, *knl_VChunkAdd;
extern char *knl_text;
extern cl_mem **clchunks;
#define COVERED(A,B,C,D,E,F) CoveredCl(A,B,C,D,E,F)
#define COVEREDFAST(A,B,C,D,E,F,G,H) CoveredCl(A,B,C,D,E,G,H)
#define DROPPOINTS(A,B,C,D,E,F) DropPointsCl(A,B,C,D,E,F)
#else
#define COVERED(A,B,C,D,E,F) Covered(A,C,D,E,F)
#define COVEREDFAST(A,B,C,D,E,F,G,H) CoveredFast(A,C,D,E,F,G,H)
//#define COVEREDFAST(A,B,C,D,E,F,G) Covered(A,C,D,E,F,G)
#define DROPPOINTS(A,B,C,D,E,F) DropPoints(A,B,C,D,E,F)
#endif

/**************************************************************************

      Functions in pmccore.c

**************************************************************************/

size_t FindRules(Edges *limits, int32 nout, size_t ncol,
                 size_t *comb, KChunk ***chunks, double mincov,
                 EdgesCollector **edcoll_ref, size_t nextract,
                 size_t *errmaxv, char **cindext);
char FindRule(Edges *limits, Edges *ed, int32 currout, int32 nout,
              size_t ncol, size_t nextract, size_t *comb,
              KChunk ***chunks, char **cind, char **cindext, size_t pattind,
              double mincov, EdgesCollector **edcoll_ref, size_t *errmaxv);
char VerifyOverDim(size_t *inds, size_t nind, Edges *edges, Edges *limits);
size_t FindFirst(char **cind1, char **cind2, int32 nout, size_t *comb,
                 size_t nextract, KChunk ***chunks);
void DropPoints(char **cind, int32 nout, Edges *edges, size_t *inds,
                size_t ninds, KChunk ***chunks);
void DropPointsAllRules(char **cind, int32 nout, EdgesCollector *edcoll,
                        KChunk ***chunks);
char *PrintRules(Edges *ed, size_t *comb, CharArray *names);
size_t CoveredOld(int32 nout, Edges *edges, size_t *inds, size_t ninds,
                  KChunk ***chunks);
char CoveredFast(register int32 nout, register Edges *edges,
                 register size_t *inds, register size_t ninds,
                 register size_t countmax, register KChunk ***chunks,
                 size_t *count_ref);
size_t Covered(register int32 nout, register Edges *edges,
               register size_t *inds, register size_t ninds,
               register KChunk ***chunks);
void ApplyRule(int32 nout, Edges *edges, size_t ncol, KChunk ***chunks);
char VerifyEd(Edges *edges, int32 outclass, int32 noutclass, size_t ncol,
              size_t *inds, size_t ninds, KChunk ***chunks, char **cind,
              size_t *errmaxv);
void ApplyRules(size_t *errtr_ref, size_t *errval_ref, int32 nout,
                EdgesCollector *edcoll, size_t ncol, KChunk ***chunks);
void ApplyRule(int32 nout, Edges *edges, size_t ncol, KChunk ***chunks);
char FindEdges(Edges *edges, int32 nout, int32 noutclass, size_t ncol,
               size_t ncoltot, Edges *limits, size_t *inds, size_t ninds,
               KChunk ***chunks, char ** cind, size_t pattind,
               size_t *errmaxv);
void FindLimits(KChunk ***chunks, Edges *limits, size_t ncol,
                int32 nout);
void FindMaxMin(KChunk *c, int32 *max_ref, int32 *min_ref);
size_t Fact(size_t n);
size_t FactNK(size_t n, size_t k);
void PrintComb(size_t *comb, size_t k);
size_t NextComb(size_t *comb, size_t k, size_t n);
void Combs(size_t ***combs_ref, size_t *ncomb_ref, size_t n, size_t k);
size_t EstimateSample(double cov, double probthresh);
size_t CountInd(char** ind, size_t nout, KChunk ***chunks);
void Ind1EqInd1ANDInd2(char** ind1, char** ind2, size_t nout, KChunk ***chunks);

/**************************************************************************

      Functions in pmccl.c

**************************************************************************/

#ifdef OPENCL
void InitCl();
char LoadChunksToDevice(KChunk ***chunks, size_t nrow, int32 nout);
char FreeChunksOnDevice(size_t nrow, int32 nout);
size_t CoveredCl(int32 nout, int32 nouttot, Edges *edges, size_t *inds,
                size_t ninds, KChunk ***chunks);
size_t CoveredClFast(int32 nout, int32 nouttot, Edges *edges, size_t *inds,
                     size_t ninds, size_t countmax,  KChunk ***chunks);
void ApplyRuleCl(cl_mem clv, int32 nout, Edges *edges, size_t *inds,
                 size_t ninds, char additive);
void DropPointsCl(char **cind, int32 nout, Edges *edges, size_t *inds,
                  size_t ninds, KChunk ***chunks);
#endif

/**************************************************************************

      Functions in pmcmem.c

**************************************************************************/

float FindNEl(float *v, size_t ind);
void CharReplace(char *str, char origc, char newc);
int32 CmpFloat(const void *a, const void * b);
int32 CmpInt32(const void *a, const void * b);
size_t FindElChArray(CharArray *carray, char *str);
void ResizeCharArray(CharArray *carray, size_t size);
size_t AddStrToCharArray(char *str, CharArray *carray, char checkdup);
void FreeCharArray(CharArray *carray);
void InitCharArrray(CharArray *carray);
Rules *InitRules(int32 nout);
void FreeRules(Rules *rules, int32 nout);
void ResizeRules(Rules *rules, size_t size);
void AppendRule(int32 nclass, size_t ncond, char* str, Rules *rules);
EdgesCollector *AppendEd(Edges *ed, int32 nclass, size_t nextract,
                         double cov, double err, size_t *comb, size_t ncol,
                         EdgesCollector *edcoll);
KChunk *ResizeChunk(KChunk *p, size_t size);
EdgesCollector *ResizeEdgesColletor(EdgesCollector *p, size_t size);
void FreeEdgesCollector(EdgesCollector *p);
size_t FindIndSize_T(size_t *v, size_t el, size_t maxlen);
size_t FindIndInt32(int32 *v, int32 el, size_t maxlen);
size_t FindIndFloat(float *v, float el, size_t maxlen);
char *FindNewEl(char *filebuf, char el);
char **InitChunksInd(int32 nout, KChunk ***chunks, size_t nsample);
void SetChunksInd(char **ind, int32 nout, KChunk ***chunks,
                  size_t nsample);
void FreeChunksInd(char ** chunksind, int32 nout);
Edges *InitEdges(size_t nrow);
void FreeEdges(Edges *lim);
void InitVoidChunks(KChunk ****chunks_ref, size_t nrow, int32 nout);
void InitChunks(KChunk ****chunks_ref, size_t nrow, size_t nelem,
                size_t *noutclass, int32 nout, char *types);
void FreeCombs(size_t **combs, size_t ncomb);
void SetTrValChunks(KChunk ***chunks, size_t nrow, int32 nout,
                    double validperc);
void FreeChunks(KChunk ***chunks, size_t nrow, size_t *noutclass,
                int32 nout);
void Error();

/**************************************************************************

      Functions in pmcfile.c

**************************************************************************/

void LoadHeterogeneousFile(char *filename, size_t *ncol_ref, char **type_ref,
                           char sepc, size_t **ind_ref,
                           size_t *nline_ref, CharArray *names_ref,
                           int argnames, int argdiscint, char *argmiss,
                           Elem32 ***col_ref, CharArray *carray_ref);
void LoadHeterogeneousBuf(size_t ncol, char *type, char *filebuf,
                          char sepc, size_t *ind, size_t nline,
                          CharArray *names_ref, int argnames,
                          int argdiscint, char *argmiss, Elem32 **col,
                          CharArray *carray_ref);
void LogRulesToHd(Rules *rules, size_t *comb, size_t ncomb, int32 nout);
void LoadChunkFromFile(KChunk **chunk_ref, int32 currout, size_t ncol);
char **LoadChunkIndFromFile(KChunk ***chunks, int32 nout);
void ShiftCharP(char **charp_ref);
char *ReadLine();
char FileLoad(char *filename, char **buf, size_t *size);
size_t CountLines(char *filebuf);
int32 FindLineOut(char *linebuf, size_t outpos, char sepc);
char *FindElOut(char *linebuf, size_t outpos, char sepc);
CharArray *LoadOut(char *filebuf, size_t ncol, size_t nline, size_t outpos,
                   char sepc, size_t **noutclass_ref, int32 *nout_ref);
void CountNOut(char *filebuf, size_t nrow, size_t nline,
               int32 **outclass_ref, size_t **noutclass_ref,
               int32 *nout_ref);
size_t CountCol(char *filebuf, char sep);
void FillChunks(KChunk ***chunks, char *filebuf, size_t nrow, size_t nline,
                size_t outpos, CharArray *outnames, size_t *ind, int32 nout);
KChunk ***LoadChunks(char *filename, size_t *ncol_ref, size_t *nelem_ref,
                     size_t **noutclass_ref, int32 *nout_ref,
                     CharArray *names, int shortname, CharArray *outnames,
                     int verbose) ;

/**************************************************************************

      Functions in pmclexer.l

**************************************************************************/

int Parse(char *filename);

/**************************************************************************

      Functions in pmcmapred.cpp

**************************************************************************/

#ifndef HADOOP
//void PmcMap(char *charp);
#endif
