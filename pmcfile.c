/**************************************************************************
          pmcfile.c  -  Functions for file loading for PMC algorithm
          --------------------------------------------------------
    begin                : Sept 25 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "pmc.h"

void LoadHeterogeneousFile(char *filename, size_t *ncol_ref, char **type_ref,
                           char sepc, size_t **ind_ref,
                           size_t *nline_ref, CharArray *names_ref,
                           int argnames, int argdiscint, char *argmiss,
                           Elem32 ***col_ref, CharArray *carray_ref) {
  size_t fsize, i;
  char *filebuforig, *filebuf;
  size_t nline = *nline_ref;
  size_t ncol = *ncol_ref;
  char *type = *type_ref;
  size_t *ind = *ind_ref;
  Elem32 **col = *col_ref;

  FileLoad(filename, &filebuf, &fsize);
  filebuforig = filebuf;
  nline = CountLines(filebuf);
  ncol = CountCol(filebuf, sepc);
  type = (char *)malloc(ncol);
  col = (Elem32 **)malloc(ncol*sizeof(int32 *));
  ind = (size_t *)malloc(ncol*sizeof(size_t));
  for (i=0; i<ncol; i++)
    col[i] = (Elem32 *)malloc((nline-1)*sizeof(int32));

  LoadHeterogeneousBuf(ncol, type, filebuf, sepc, ind,
                       nline, names_ref, argnames,
                       argdiscint, argmiss, col, carray_ref);

  *nline_ref = nline;
  *ncol_ref = ncol;
  *col_ref = col;
  *ind_ref = ind;
  *type_ref = type;

  free(filebuforig);

  return;
}

void LoadHeterogeneousBuf(size_t ncol, char *type, char *filebuf,
                          char sepc, size_t *ind, size_t nline,
                          CharArray *names_ref, int argnames,
                          int argdiscint, char *argmiss, Elem32 **col,
                          CharArray *carray_ref) {
  size_t i, j, k, ctmp, outpos;
  char chtmp;

  /* Load input types */

  for (i=0; i<ncol-1; i++) {
    type[i] = filebuf[0];
    filebuf = FindNewEl(filebuf, sepc);
  }
  type[ncol-1] = filebuf[0];
  filebuf = FindNewEl(filebuf, '\n');
  j = 0;
  for (i=0; i<ncol; i++) {
    if (type[i]=='o') {
      j = 1;
      k = i;
      outpos = i;
    }
    else
      ind[i]=i-j;
  }
  ind[outpos] = ncol-1;
  nline--;

  /* Load input names if requested */

  InitCharArrray(names_ref);
  ResizeCharArray(names_ref, ncol);
  if (argnames) {
    char outname[MAXSTRING];

    for (i=0; i<ncol; i++) {
      for (k=0; filebuf[k]!=sepc && filebuf[k]!='\n'; k++)
        ;
      chtmp = filebuf[k];
      filebuf[k] = '\0';
      if (type[i]=='o')
        sprintf(outname, "%s#%ld", filebuf, i);
      else {
        char str[MAXSTRING];
        sprintf(str, "%s#%ld", filebuf, i);
        if (type[i]=='i') {
          strcat(str, "#i");
          if (argdiscint)
            strcat(str, "d");
        }
        else if (type[i]=='f')
          strcat(str, "#f");
        AddStrToCharArray(str, names_ref, 0);
      }
      filebuf[k] = chtmp;
      if (i!=ncol-1)
        filebuf = FindNewEl(filebuf, sepc);
    }
    /* strcat(outname, "#o"); */
    AddStrToCharArray(outname, names_ref, 0);
    filebuf = FindNewEl(filebuf, '\n');
    nline--;
  }
  else {
    char str[MAXSTRING];

    for (i=0; i<ncol; i++) {
      if (type[i]!='o') {
        sprintf(str, STR(INPUTDEFAULTNAME), ind[i]);
        if (type[i]=='i') {
          strcat(str, "#i");
          if (argdiscint)
            strcat(str, "d");
        }
        else if (type[i]=='f')
          strcat(str, "#f");
        AddStrToCharArray(str, names_ref, 0);
      }
    }
    sprintf(str, "OUT#%ld", outpos);
    AddStrToCharArray(str, names_ref, 0);
  }


  for (i=0; i<nline; i++)
    for (j=0; j<ncol; j++) {
      for (k=0; filebuf[k]!=sepc && filebuf[k]!='\n'; k++)
        ;
      chtmp = filebuf[k];
      filebuf[k] = '\0';
      if (strcmp(filebuf, argmiss))
        switch (type[j])
        {
        case 'i':
          col[ind[j]][i].i = atoi(filebuf);
          break;
        case 'f':
          col[ind[j]][i].f = atof(filebuf);
          break;
        case 'o':
          if (i<nline-1) {
            ctmp = AddStrToCharArray(filebuf, carray_ref, 1);
            col[ncol-1][i].i = ctmp;
          }
          else {
            ctmp = AddStrToCharArray(filebuf, carray_ref, 1);
            col[ncol-1][i].i = ctmp;
          }
          break;
        default :
          fprintf(stderr, "Invalid type for column %ld...\n", j);
          exit(1);
        }
      else
        col[ind[j]][i].i = MISSVAL;

      filebuf[k] = chtmp;
      if (j!=ncol-1)
        filebuf = FindNewEl(filebuf, sepc);
      else
        filebuf = FindNewEl(filebuf, '\n');
    }

  return;
}

void LogRulesToHd(Rules *rules, size_t *comb, size_t ncomb, int32 nout) {
  size_t i, j;
  char filename[MAXSTRING], buf[MAXSTRING];
  FILE *pfile;

  strcpy(filename, STR(TEMPFILEDIR)
         STR(RULESFILENAME1));
  for (i=0; i<ncomb; i++){
    sprintf(buf, STR(RULESFILENAME2), comb[i]);
    strcat(filename, buf);
  }
  strcat(filename, STR(RULESFILENAME3));


  pfile = fopen(filename, "wb");
  if (!pfile)
    Error();
  for (i=0; i<nout; i++) {
    if (rules[i].len) {
      printf("Printing rules for output number: %ld\n", i);
      for (j=0; j<rules[i].len; j++) {
        printf("%s", rules[i].v[j]);
        fprintf(pfile, "%s", rules[i].v[j]);
      }
    }
  }
  fclose(pfile);
}

void LoadChunkFromFile(KChunk **chunk_ref, int32 currout, size_t ncol) {
  size_t len, chunksize;
  char chunkname[MAXSTRING];
  FILE *pfile;
  KChunk *chunk = NULL;

  sprintf(chunkname, STR(TEMPFILEDIR)
          STR(CHUNKFILENAME), currout, ncol);
  pfile = fopen(chunkname, "r");
  if (!pfile)
    Error();
  if (fread(&len, sizeof(size_t), 1, pfile) != 1)
    Error();
  rewind(pfile);
  chunksize = sizeof(KChunk)+(len-1)*sizeof(int32);
  chunk = ResizeChunk(chunk, chunksize);
  if (fread(chunk, chunksize, 1, pfile) != 1)
    Error();
  fclose(pfile);
  *chunk_ref = chunk;
}

char **LoadChunkIndFromFile(KChunk ***chunks, int32 nout) {
  int32 i;
  char filename[MAXSTRING];
  FILE *pfile;
  char **cind;

  cind = InitChunksInd(nout, chunks, -1);

  for (i=0; i<nout; i++) {
    sprintf(filename, STR(TEMPFILEDIR)
            STR(CHUNKINDFILENAME), i);

    pfile = fopen(filename, "r");
    if (fread(cind[i], chunks[i][0]->len, 1, pfile) != 1)
      Error();

    fclose(pfile);
  }

  return cind;
}

void ShiftCharP(char **charp_ref) {
  char *charp;

  charp = *charp_ref;
  charp++;
  while (*charp != ' ' && *charp != '\0')
    charp++;
  *charp_ref = charp;
}

/* Read a line from stdin. C makes things simple. :)
 * From http://stackoverflow.com/a/314422/1148634
 */
char *ReadLine()
{
  char *linen;
  char *line = (char *) malloc(MAXSTRING), * linep = line;
  size_t lenmax = MAXSTRING, len = lenmax;
  int c;

  if(line == NULL)
    return NULL;

  for(;;)
  {
    c = fgetc(stdin);
    if(c == EOF)
      break;

    if(--len == 0)
    {
      linen = (char *) realloc(linep, lenmax *= 2);
      len = lenmax;

      if(linen == NULL)
      {
        free(linep);
        return NULL;
      }
      line = linen + (line - linep);
      linep = linen;
    }

    if((*line++ = c) == '\n')
      break;
  }
  *line = '\0';
  return linep;
}

char FileLoad(char *filename, char **buf, size_t *size) {
  size_t pos;
  FILE *f;
  char *filebuf = NULL;
  if (strcmp(filename, "-")) {
    f = fopen(filename, "rb");
    if (f) {
      fseek(f, 0, SEEK_END);
      pos = ftell(f);
      fseek(f, 0, SEEK_SET);
      filebuf = (char *)malloc(pos+1);
      fread(filebuf, 1, pos, f);
      fclose(f);
      filebuf[pos] = '\0';
      *size = pos+1;
      *buf = filebuf;
      return 0;
    }
    else
      return 1;
  }
  // Load data from stdin
  else {
    pos = 0;
    *size = MAXSTRING;
    filebuf = (char *)malloc(*size);
    while ((filebuf[pos++] = getc(stdin)) != EOF) {
      if (pos==*size) {
        *size *=2;
        filebuf = realloc(filebuf, *size);
      }
    }
    filebuf[pos-1] = '\0';
    *size = pos;
    *buf = filebuf;
  }
  return 0;
}

size_t CountLines(char *filebuf) {
  size_t i;
  size_t lines=0;

  for (i=0; filebuf[i+1]!='\0'; i++)
    if (filebuf[i]=='\n')
      lines++;
  return ++lines;
}

int32 FindLineOut(char *linebuf, size_t outpos, char sepc) {
  size_t i;
  size_t seps=0;

  for (i=0; linebuf[i+1]!='\n'; i++) {
    if (linebuf[i]==sepc)
      seps++;
    if (seps == outpos)
      break;
  }
  return atoi(linebuf+i+0);
}

char *FindElOut(char *linebuf, size_t outpos, char sepc) {
  size_t i;
  char chtmp;
  char *str;

  if (outpos)
    for (i=0; i<outpos-1; i++)
      linebuf = FindNewEl(linebuf, sepc);

  for (i=0; linebuf[i]!=sepc && linebuf[i]!='\n'; i++)
    ;
  chtmp = linebuf[i];
  linebuf[i] = '\0';
  str = (char *)malloc(MAXSTRING);
  strcpy(str, linebuf);
  linebuf[i] = chtmp;

  return str;
}

CharArray *LoadOut(char *filebuf, size_t ncol, size_t nline, size_t outpos,
                   char sepc, size_t **noutclass_ref, int32 *nout_ref) {
  int32 i, j;
  char chtmp;
  size_t *noutclass;
  CharArray *charray;

  charray = (CharArray *)malloc(sizeof(CharArray));
  noutclass = (size_t *)calloc(sizeof(size_t), MAXCLASSOUT);

  InitCharArrray(charray);

  for (i=0; i<nline; i++) {
    if (outpos)
      for (j=0; j<outpos-1; j++)
        filebuf = FindNewEl(filebuf, sepc);
    for (j=0; filebuf[j]!=sepc && filebuf[j]!='\n'; j++)
      ;
    chtmp = filebuf[j];
    filebuf[j] = '\0';
    noutclass[AddStrToCharArray(filebuf, charray, 1)]++;
    filebuf[j] = chtmp;
    if (i!=nline)
      filebuf = FindNewEl(filebuf, '\n');
  }

  *nout_ref = charray->len;
  *noutclass_ref = noutclass;

  return charray;
}

void CountNOut(char *filebuf, size_t ncol, size_t nline,
               int32 **outclass_ref, size_t **noutclass_ref,
               int32 *nout_ref) {
  int32 i, out, ic;
  int32 *outclass;
  size_t *noutclass;
  int32 nout = 0;

  outclass = (int32 *)calloc(sizeof(int32), MAXCLASSOUT);
  noutclass = (size_t *)calloc(sizeof(size_t), MAXCLASSOUT);

  for (i=0; i<=nline; i++) {
    out = FindLineOut(filebuf, ncol, '\t');
    ic = FindIndInt32(outclass, out, MAXCLASSOUT);
    if (ic==-1) {
      ic = ++nout;
      outclass[nout] = out;
    }
    else
      outclass[ic] = out;
    noutclass[ic]++;
    if (i!=nline)
      filebuf = FindNewEl(filebuf, '\n');
  }
  *outclass_ref = outclass;
  *noutclass_ref = noutclass;
  *nout_ref = nout+1;
}

size_t CountCol(char *filebuf, char sep) {
  size_t i;
  size_t nsep = 0;

  for (i=0; filebuf[i+1]!='\n'; i++)
    if (filebuf[i]==sep)
      nsep++;
  return ++nsep;
}

void FillChunks(KChunk ***chunks, char *filebuf, size_t ncol, size_t nline,
                size_t outpos, CharArray *outnames, size_t *ind,
                int32 nout) {
  int32 ic;
  size_t i, j, k;
  char *pline, *pel, *out;

  pel = pline = filebuf;

  for (i=0; i<nline; i++) {
    if (i)
      pel = pline = FindNewEl(pline, '\n');
    /* out = FindLineOut(pline, ncol); */
    out = FindElOut(pline, outpos, '\t');
    for (j=0; j<ncol-1; j++) {
      k = ind[j];
      if (j)
        pel = FindNewEl(pel, '\t');
      /* ic = FindIndInt32(noutclass, out, MAXCLASSOUT); */
      ic = FindElChArray(outnames, out);
      if (ic==-1)
        Error();
      if (chunks[ic][k]->type==INT)
        chunks[ic][k]->v[(chunks[ic][k]->len)++].i = atoi(pel);
      else
        chunks[ic][k]->v[(chunks[ic][k]->len)++].f = atof(pel);
    }
    free(out);
  }
}

KChunk ***LoadChunks(char *filename, size_t *ncol_ref, size_t *nelem_ref,
                     size_t **noutclass_ref, int32 *nout_ref,
                     CharArray *names, int shortname, CharArray *outnames,
                     int verbose) {
  char chtmp;
  int32 nout;
  size_t ncol, nelem, fsize, i, j, k, outpos;
  char str[MAXSTRING];
  char *filebuf, *filebuforig, *types;
  size_t *noutclass, *ind;
  KChunk ***chunks;

  FileLoad(filename, &filebuf, &fsize);
  filebuforig = filebuf;
  ncol = CountCol(filebuf, '\t');

  types = (char *) alloca(ncol);
  ind = (size_t *) alloca(ncol*sizeof(size_t));

  /* Load input types */

  for (i=0; i<ncol; i++) {
    types[i] = filebuf[0];
    if (i!=ncol-1)
      filebuf = FindNewEl(filebuf, '\t');
    else
      filebuf = FindNewEl(filebuf, '\n');
  }
  types[ncol] = filebuf[0];
  /* filebuf = FindNewEl(filebuf, '\n'); */
  j = 0;
  for (i=0; i<ncol+1; i++) {
    if (types[i]=='o') {
      j = 1;
      k = i;
      outpos = i+1;
    }
    else
      ind[i]=i-j;
  }
  ind[outpos] = ncol-1;
  /* nline--; */

  /* Read input names line */
  InitCharArrray(names);
  ResizeCharArray(names, ncol);
  for (i=0; i<ncol-1; i++) {
    for (k=0; filebuf[k]!='\t' && filebuf[k]!='\n'; k++)
      ;
    chtmp = filebuf[k];
    filebuf[k] = '\0';
    strcpy(str, filebuf);
    if (shortname)
      CharReplace(str, '#', '\0');
    AddStrToCharArray(str, names, 0);
    filebuf[k] = chtmp;
    filebuf += k+1;
  }
  filebuf = FindNewEl(filebuf, '\n');
  nelem = CountLines(filebuf);

  outnames = LoadOut(filebuf, ncol, nelem, outpos, '\t', &noutclass,
                     &nout);
  /* CountNOut(filebuf, ncol, nelem, &outclass, &noutclass, &nout); */

  InitChunks(&chunks, ncol, nelem, noutclass, nout, types);
  FillChunks(chunks, filebuf, ncol, nelem, outpos, outnames, ind, nout);
  //DiscretizeChunks()
  if (verbose) {
    printf("file has %ld lines\n", nelem+1);
    printf("file has %ld cols\n", ncol+1);
  }

  free(filebuforig);

  *ncol_ref = ncol;
  *nelem_ref = nelem;
  *noutclass_ref = noutclass;
  *nout_ref = nout;

  return chunks;
}
