/**************************************************************************
          pmc-hadoop.cpp  -  hadoop main for PMC algorithm
          --------------------------------------------------------
    begin                : Nov 18 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "pmc.h"
#include "hadoop/Pipes.hh"
#include "hadoop/TemplateFactory.hh"
#include "hadoop/StringUtils.hh"

extern "C" { void PmcMap(char *charp); }

const std::string PMC_MAP = "PMC_MAP";
const std::string PMC_RED = "PMC_RED";
const std::string PMC_MAP_COUNT = "PMC_MAP_COUNT";
const std::string PMC_RULES_GENERATED = "PMC_RULES_GENERATED";
const std::string PMC_RULES_COLLECTED = "PMC_RULES_COLLECTED";

HadoopPipes::MapContext *globcontext;

extern "C" void HadoopEmitRule(Rules *rules, size_t *comb, size_t ncomb, int32 nout) {
  int32 i;
  size_t j;

  for (i=0; i<nout; i++)
    if (rules[i].len)
      for (j=0; j<rules[i].len; j++) {
        std::string rule(rules[i].v[j]);
        globcontext->emit("1", rule);
      }
}
extern "C" void HadoopIncrementRuleCount() {
  HadoopPipes::TaskContext::Counter* rgencount;

  rgencount = globcontext->getCounter(PMC_MAP, PMC_RULES_GENERATED);
  globcontext->incrementCounter(rgencount, 1);
}

class WordCountMap: public HadoopPipes::Mapper {
public:
  HadoopPipes::TaskContext::Counter* jobcount;


  WordCountMap(HadoopPipes::TaskContext& context) {
    jobcount = context.getCounter(PMC_MAP, PMC_MAP_COUNT);
  }

  void map(HadoopPipes::MapContext& context) {
    char *charp;
    globcontext = &context;
    std::string line = context.getInputValue();

    charp = (char *)alloca(line.length()+1);
    strcpy(charp, line.c_str());
    PmcMap(charp);

    context.incrementCounter(jobcount, 1);
  }
};

class WordCountReduce: public HadoopPipes::Reducer {
public:
  HadoopPipes::TaskContext::Counter* outputrules;

  WordCountReduce(HadoopPipes::TaskContext& context) {
    outputrules = context.getCounter(PMC_RED, PMC_RULES_COLLECTED);
  }

  void reduce(HadoopPipes::ReduceContext& context) {
    // std::vector<std::string> stringarray;

    // while (context.nextValue()) {
    //   sum += HadoopUtils::toInt(context.getInputValue());
    //   stringarray.assign(i++, context.getInputValue());
    // }
    // stringarray.assign(i++, context.getInputKey());
    while (context.nextValue()) {
      context.emit(context.getInputValue(), "");
      context.incrementCounter(outputrules, 1);
    }

    //context.emit(context.getInputKey(), "");
    //context.incrementCounter(outputrules, 1);
  }
};

int main(int argc, char *argv[]) {
  return HadoopPipes::runTask(HadoopPipes::TemplateFactory<WordCountMap,
                              WordCountReduce>());
}
