/**************************************************************************
          pmccore.c  -  Core functions for PMC algorithm
          --------------------------------------------------------
    begin                : Sept 25 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.


**************************************************************************/

#include "pmc.h"

size_t FindRules(Edges *limits, int32 nout, size_t ncol,
                 size_t *comb, KChunk ***chunks, double mincov,
                 EdgesCollector **edcoll_ref, size_t nextract,
                 size_t *errmaxv, char **cindext) {
  int32 i;
  Edges *ed;
  char **cind;
  size_t pattind;
  size_t stepcount = 0;

  for (i=0; i<nout; i++) {
    ed = InitEdges(ncol);
    cind = InitChunksInd(nout, chunks, -1);
    pattind = FindFirst(cind, cindext, i, comb, nextract, chunks);
    while (pattind!=-1) {
#ifdef HADOOP
      HadoopIncrementRuleCount();
#endif
      stepcount++;
      if (FindRule(limits, ed, i, nout, ncol, nextract, comb, chunks, cind,
                   cindext, pattind, mincov, edcoll_ref, errmaxv))
        break;
      pattind = FindFirst(cind, cindext, i, comb, nextract, chunks);
    }
    /* reduce external seeds */
    /* Ind1EqInd1ANDInd2(cindext, cind, nout, chunks); */

    FreeChunksInd(cind, nout);
    FreeEdges(ed);
  }
  return stepcount;
}

char FindRule(Edges *limits, Edges *ed, int32 currout, int32 nout,
              size_t ncol, size_t nextract, size_t *comb,
              KChunk ***chunks, char **cind, char **cindext, size_t pattind,
              double mincov, EdgesCollector **edcoll_ref, size_t *errmaxv) {
  char res;
  size_t i, j;
  double cov;
  size_t errcount = 0, othercount = 0;
  double err = 0;

  res = FindEdges(ed, currout, nout, nextract, ncol, limits, comb,
                  nextract, chunks, cind, pattind, errmaxv);
  DROPPOINTS(cind, currout, ed, comb, nextract, chunks);
  if (!res) {
    if (VerifyOverDim(comb, nextract, ed, limits)) {
      cov = 100*(double)COVERED(currout, nout, ed, comb, nextract, chunks)/
        chunks[currout][0]->trainlen;
      for (i=0; i<nout; i++) {
        if(i!=currout) {
          errcount += COVERED(i, nout, ed, comb, nextract, chunks);
          othercount += chunks[i][0]->trainlen;
        }
      }
      err = 100*(double)errcount/othercount;
      if (cov>=mincov) {
        for (i=0; i<nextract; i++) {
          j = comb[i];
          if (ed->min[j].i==limits->min[j].i)
            ed->min[j].i = MINVAL;
          if (ed->max[j].i==limits->max[j].i)
            ed->max[j].i = MAXVAL;
          DROPPOINTS(cindext, currout, ed, comb, nextract, chunks);
        }
        *edcoll_ref =
          AppendEd(ed, currout, nextract, cov, err, comb, ncol,
                   *edcoll_ref);
      }
    }
    else
      return 0; // put 1 to activate overdim check
  }
  return 0;
}

char VerifyOverDim(size_t *inds, size_t nind, Edges *edges, Edges *limits) {
  size_t i, j;

  for (i=0; i<nind; i++) {
    j = inds[i];
    if (edges->min[j].i==limits->min[j].i && edges->max[j].i==limits->max[j].i)
      return 0;
  }
  return 1;
}

size_t FindFirst(char **cind1, char **cind2, int32 nout, size_t *comb,
                 size_t nextract, KChunk ***chunks) {
  size_t i, j, count;

  for (i=0; i<chunks[nout][0]->trainlen; i++)
    if (cind1[nout][i] && cind2[nout][i]) {
      count = 0;
      for (j=0; j<nextract; j++)
        if (chunks[nout][comb[j]]->v[i].i!=MISSVAL)
          count++;
      if (count==nextract)
        return i;
    }
  return -1;
}

void DropPoints(char **cind, int32 nout, Edges *edges, size_t *inds,
                size_t ninds, KChunk ***chunks) {
  size_t i, j, k, res;

  for (j=0; j<chunks[nout][0]->trainlen; j++) {
    if (cind[nout][j]) {
      res = 0;
      for (i=0; i<ninds; i++) {
        k = inds[i];
        if (edges->min[k].i<=chunks[nout][k]->v[j].i &&
            edges->max[k].i>=chunks[nout][k]->v[j].i)
          res++;
      }
      if (res == ninds)
        cind[nout][j] = 0;
    }
  }
  return;
}

void DropPointsAllRules(char **cind, int32 nout, EdgesCollector *edcoll,
                        KChunk ***chunks) {
  size_t i, j;

  for (i=0; i<nout; i++)
    for (j=0; j<edcoll->len; j++)
      DROPPOINTS(cind, i, edcoll->ed[j], edcoll->ed[j]->comb,
                 edcoll->ed[j]->ndim, chunks);

}

char *PrintRules(Edges *ed, size_t *comb, CharArray *names) {
  size_t i, k;
  CharArray namesalt;
  char tmp[MAXSTRING];
  char *buf;
  char tmpflag = 0;

  if (!names) {
    size_t maxcomb = 0;

    tmpflag = 1;
    names = &namesalt;
    InitCharArrray(names);
    for (i=0; i<ed->ndim; i++)
      if (ed->comb[i]>maxcomb)
        maxcomb = ed->comb[i];
    ResizeCharArray(names, maxcomb+1);

    for (i=0; i<=maxcomb; i++) {
      names->c[i] = (char *)alloca(MAXSTRING);
      sprintf(tmp, STR(INPUTDEFAULTNAME), i);
      strcpy(names->c[i], tmp);
    }
  }

  buf = (char *)calloc(MAXSTRING, sizeof(char));

  if(!comb) {
    comb = (size_t *)alloca(ed->ndim*sizeof(size_t));
    for (i=0; i<ed->ndim; i++)
      comb[i] = i;
  }

  sprintf(tmp, "%.2f%%=Cov\t%.2f%%=Err\t", ed->cov, ed->err);
  strcat(buf, tmp);

  for (i=0; i<ed->ndim; i++) {
    k = comb[i];
    if (ed->type[k]==INT) {
      if (ed->min[k].i==MINVAL && ed->max[k].i==MAXVAL)
        printf(tmp, "-inf<%s<+inf\t", names->c[ed->comb[i]]);
      else if (ed->min[k].i==MINVAL)
        sprintf(tmp, "%s<=%d\t", names->c[ed->comb[i]], ed->max[k].i);
      else if (ed->max[k].i==MAXVAL)
        sprintf(tmp, "%d<=%s\t", ed->min[k].i, names->c[ed->comb[i]]);
      else
        sprintf(tmp, "%d<=%s<=%d\t", ed->min[k].i, names->c[ed->comb[i]],
                ed->max[k].i);
    }
    else if (ed->type[k]==FLOAT) {
      if (ed->min[k].i==MINVAL && ed->max[k].i==MAXVAL)
        printf(tmp, "-inf<%s<+inf\t", names->c[ed->comb[i]]);
      else if (ed->min[k].i==MINVAL)
        sprintf(tmp, "%s<=%f\t", names->c[ed->comb[i]], ed->max[k].f);
      else if (ed->max[k].i==MAXVAL)
        sprintf(tmp, "%f<=%s\t", ed->min[k].f, names->c[ed->comb[i]]);
      else
        sprintf(tmp, "%f<=%s<=%f\t", ed->min[k].f, names->c[ed->comb[i]],
                ed->max[k].f);
    }
    strcat(buf, tmp);
  }
  sprintf(tmp, "OUT=%d\n", ed->nclass);
  strcat(buf, tmp);

  if (tmpflag)
    free(names->c);

  return buf;
}

size_t CoveredOld(int32 nout, Edges *edges, size_t *inds, size_t ninds,
                  KChunk ***chunks) {
  size_t i, j, k, res;
  size_t count = 0;

  for (j=0; j<chunks[nout][0]->trainlen; j++) {
    res = 0;
    for (i=0; i<ninds; i++) {
      k = inds[i];
      if (chunks[nout][k]->v[j].i==MISSVAL ||
          (edges->min[k].i<=chunks[nout][k]->v[j].i &&
           edges->max[k].i>=chunks[nout][k]->v[j].i))
        res++;
    }
    if (res == ninds)
      count++;
  }
  return count;
}

char CoveredFast(register int32 nout, register Edges *edges,
                 register size_t *inds, register size_t ninds,
                 register size_t errmax, register KChunk ***chunks,
                 size_t *count_ref) {
  register size_t i, k, res;
  register int32 *pend;
  register int32 **p;
  register size_t count = 0;

  p = alloca(ninds*sizeof(int32 *)+63);
  p = (int32 **)(((uintptr_t)p+63)&~(uintptr_t)0x3F);

  for (i=0; i<ninds; i++)
    p[i] = (int32 *)chunks[nout][inds[i]]->v;
  pend = (int32 *)chunks[nout][inds[0]]->v+chunks[nout][inds[0]]->trainlen;

  while (*p<pend) {
    res = 0;
    for (i=0; i<ninds; i++) {
      k = inds[i];
      if ((edges->min[k].i<=*p[i] && edges->max[k].i>=*p[i]) ||
          *p[i]==MISSVAL)
        res++;
      p[i]++;
    }
    if (res==ninds && count++==errmax)
      return 1;
  }
  *count_ref = count;
  return 0;
}

size_t Covered(register int32 nout, register Edges *edges,
               register size_t *inds, register size_t ninds,
               register KChunk ***chunks) {
  register size_t i, k, res;
  register int32 *pend;
  register int32 **p;
  register size_t count = 0;

  p = alloca(ninds*sizeof(int32 *)+63);
  p = (int32 **)(((uintptr_t)p+63)&~(uintptr_t)0x3F);

  for (i=0; i<ninds; i++)
    p[i] = (int32 *)chunks[nout][inds[i]]->v;
  pend = (int32 *)chunks[nout][inds[0]]->v+chunks[nout][inds[0]]->trainlen;

  while (*p<pend) {
    res = 0;
    for (i=0; i<ninds; i++) {
      k = inds[i];
      if (*p[i]==MISSVAL ||
          (edges->min[k].i<=*p[i] && edges->max[k].i>=*p[i]))
        res++;
      p[i]++;
    }
    if (res == ninds)
      count++;
  }
  return count;
}

void ApplyRules(size_t *errtr_ref, size_t *errval_ref, int32 nout,
                EdgesCollector *edcoll, size_t ncol, KChunk ***chunks) {
  size_t i, j, k;
  int32 maxcol;

  for (j=0; j<nout; j++)
    for (i=ncol; i<ncol+nout+1; i++)
      memset(chunks[j][i]->v, -1, chunks[j][0]->len*sizeof(int32));

  for (i=0; i<edcoll->len; i++)
    ApplyRule(nout, edcoll->ed[i], ncol, chunks);

  for (j=0; j<nout; j++)
    for (k=0; k<chunks[j][0]->len; k++) {
      maxcol = rand()%nout;
      for (i=ncol; i<ncol+nout; i++)
        if (chunks[j][i]->v[k].i>maxcol)
          maxcol = i-ncol;
      chunks[j][ncol+nout]->v[k].i = maxcol;
      if (chunks[j][ncol+nout]->v[k].i!=j) {
        if (k<chunks[j][0]->trainlen)
          (*errtr_ref)++;
        else
          (*errval_ref)++;
      }
    }
  return;
}

void ApplyRule(int32 nout, Edges *edges, size_t ncol, KChunk ***chunks) {
  register size_t i, j, k, res;
  register int32 *pend;
  register int32 **p;

  p = alloca(edges->ndim*sizeof(int32 *)+63);
  p = (int32 **)(((uintptr_t)p+63)&~(uintptr_t)0x3F);

  for (j=0; j<nout; j++) {
    for (i=0; i<edges->ndim; i++)
      p[i] = (int32 *)chunks[j][edges->comb[i]]->v;
    pend = (int32 *)chunks[j][edges->comb[0]]->v+
      chunks[j][edges->comb[0]]->len;

    while (*p<pend) {
      res = 0;
      for (i=0; i<edges->ndim; i++) {
        k = edges->comb[i];
        if (edges->min[k].i<=*p[i] &&
            edges->max[k].i>=*p[i])
          res++;
        p[i]++;
      }
      if (res == edges->ndim)
        chunks[j][ncol+edges->nclass]->v[chunks[j][0]->len-(pend-*p)-1].i +=
          (int32)(edges->cov);
    }
  }
}

char VerifyEd(Edges *edges, int32 outclass, int32 noutclass, size_t ncol,
              size_t *inds, size_t ninds, KChunk ***chunks, char **cind,
              size_t *errmaxv) {
  size_t nout, errtmp, count;// a, b;

  if (errmaxv[outclass])
    errtmp = errmaxv[outclass]*
      COVERED(outclass, noutclass, edges, inds, ninds, chunks)/
      chunks[outclass][0]->trainlen;
  else
    errtmp = 0;

  for (nout=0; nout<noutclass; nout++)
    if (nout!=outclass) {
      /* a = CoveredCl(nout, noutclass, edges, inds, ninds, errmax, chunks); */
      /* b = CoveredFast(nout, edges, inds, ninds, errmax, chunks); */
      /* if (a) */
      /*   a = 1 ; */
      /* if (a!=b) */
      /*   printf("mumble..."); */
      if (COVEREDFAST(nout, noutclass, edges, inds, ninds, errtmp, chunks, &count)
          !=0)
        return 0;
      errtmp -= count;
    }
  return 1;
}

char FindEdges(Edges *edges, int32 nout, int32 noutclass, size_t ncol,
               size_t ncoltot, Edges *limits, size_t *inds, size_t ninds,
               KChunk ***chunks, char ** cind, size_t pattind,
               size_t *errmaxv) {
  size_t i, j, dimtouched;
  Edges *steps;
  size_t res = 1;
  MinMax minmax = UNKNOWN;


  for (i=0; i<ninds; i++) {
    j = inds[i];
    edges->min[j].i = edges->max[j].i = chunks[nout][j]->v[pattind].i;
  }
  steps = InitEdges(ncoltot);
  for (i=0; i<ninds; i++) {
    j = inds[i];
    steps->max[j].i = steps->min[j].i = 1;
  }
#ifdef OPENCL
  growing = 1;
#endif

  while (res) {
    res = 0;
    for (i=0; i<ninds; i++) {
      j = inds[i];

      if (limits->min[j].i<=edges->min[j].i-steps->min[j].i) {
        res = 1;
        edges->min[j].i -= steps->min[j].i;
        if (!VerifyEd(edges, nout, noutclass, ncol, inds, ninds, chunks,
                      cind, errmaxv)) {
          edges->min[j].i += steps->min[j].i;
          minmax = MIN;
          res = 0;
        }
        else
          steps->min[j].i *= 2;
      }
      if (edges->min[j].i<=limits->min[j].i) {
        minmax = MIN;
        res = 0;
        break;
      }
      if (edges->max[j].i+steps->max[j].i<=limits->max[j].i) {
        res = 1;
        edges->max[j].i += steps->max[j].i;
        if (!VerifyEd(edges, nout, noutclass, ncol, inds, ninds, chunks,
                      cind, errmaxv)) {
          edges->max[j].i -= steps->max[j].i;
          minmax = MAX;
          res = 0;
        }
        else
          steps->max[j].i *= 2;
      }
      if (edges->max[j].i>=limits->max[j].i) {
        minmax = MAX;
        res = 0;
        break;
      }
    }
  }
  dimtouched = j;
#ifdef OPENCL
  growing = 0;
#endif
  for (i=0; i<ninds; i++) {
    j = inds[i];
    if (!(j==dimtouched && minmax==MIN))
      steps->min[j].i = (limits->max[j].i-limits->min[j].i)/FRACTSTEP;
    if (!(j==dimtouched && minmax==MAX))
      steps->max[j].i = (limits->max[j].i-limits->min[j].i)/FRACTSTEP;
  }

  res = 1;
  while (res) {
    for (i=0; i<ninds; i++) {
      j = inds[i];
      if (steps->min[j].i!=0 &&
          limits->min[j].i<=edges->min[j].i-steps->min[j].i) {
        edges->min[j].i -= steps->min[j].i;
        if (!VerifyEd(edges, nout, noutclass, ncol, inds, ninds, chunks,
                      cind, errmaxv)) {
          edges->min[j].i += steps->min[j].i;
          steps->min[j].i /= 2;
        }
      }
      else
        steps->min[j].i /= 2;

      if (steps->max[j].i!=0 &&
          edges->max[j].i+steps->max[j].i<=limits->max[j].i) {
        edges->max[j].i+= steps->max[j].i;
        if (!VerifyEd(edges, nout, noutclass, ncol, inds, ninds, chunks,
                      cind, errmaxv)) {
          edges->max[j].i -= steps->max[j].i;
          steps->max[j].i /= 2;
        }
      }
      else
        steps->max[j].i /= 2;
    }
    res = 2*ncol;
    for (i=0; i<ninds; i++) {
      j = inds[i];
      if (steps->min[j].i==0)
        res--;
      if (steps->max[j].i==0)
        res--;
    }
  }

  FreeEdges(steps);
  if(VerifyEd(edges, nout, noutclass, ncol, inds, ninds, chunks,
              cind, errmaxv))
    return 0;
  else
    return 1;
}

void FindLimits(KChunk ***chunks, Edges *limits, size_t ncol,
                int32 nout) {
  int32 **min, **max;
  size_t i, j;

  max = (int32 **)alloca(nout*sizeof(int32 *));
  min = (int32 **)alloca(nout*sizeof(int32 *));
  for (i=0; i<nout; i++) {
    max[i] = (int32 *)alloca(ncol*sizeof(int32));
    min[i] = (int32 *)alloca(ncol*sizeof(int32));
  }
  for (i=0; i<nout; i++)
    for (j=0; j<ncol; j++)
      FindMaxMin(chunks[i][j], &(max[i][j]), &min[i][j]);

  for (j=0; j<ncol; j++) {
    limits->max[j].i = max[0][j];
    limits->min[j].i = min[0][j];
    for (i=1; i<nout; i++) {
      if (limits->max[j].i<max[i][j])
        limits->max[j].i=max[i][j];
      if (limits->min[j].i>min[i][j])
        limits->min[j].i=min[i][j];
    }
  }
}

void FindMaxMin(KChunk *c, int32 *max_ref, int32 *min_ref) {
  int32 max, min;
  size_t i;

  min = max = c->v[0].i;
  for (i=1; i<c->trainlen; i++) {
    if (min>c->v[i].i)
      min=c->v[i].i;
    if (max<c->v[i].i)
      max=c->v[i].i;
  }
  *max_ref = max;
  *min_ref = min;
}

size_t Fact(size_t n) {
  if (n==1 || n==0)
    return 1;
  else
    return n*Fact(n-1);
}

size_t FactNK(size_t n, size_t k) {
  if (n==k)
    return 1;
  else
    return n*FactNK(n-1, k);
}

/* Permutation generator originaly
   from http://dzone.com/snippets/generate-combinations */
void PrintComb(size_t *comb, size_t k) {
  printf("{");
  size_t i;
  for (i = 0; i < k; ++i)
    printf("%ld, ", comb[i]);
  printf("\b\b}\n");
}

size_t NextComb(size_t *comb, size_t k, size_t n) {
  size_t i = k-1;
  ++comb[i];
  while ((i>0) && (comb[i]>=n-k+1+i)) {
    --i;
    ++comb[i];
  }

  if (comb[0]>n-k)
    return 0;

  for (i=i+1; i<k; ++i)
    comb[i] = comb[i-1]+1;

  return 1;
}

/* Permutation without repetition of k groups in n */
void Combs(size_t ***combs_ref, size_t *ncomb_ref, size_t n, size_t k) {
  size_t i, ncomb;
  size_t *comb;
  size_t **combs;

  if (n>2*k)
    ncomb = FactNK(n, n-k)/Fact(k);
  else
    ncomb = FactNK(n, k)/Fact(n-k);

  comb = (size_t *)alloca(k*sizeof(size_t));
  combs = (size_t **)malloc(ncomb*sizeof(size_t *));
  for (i=0; i<ncomb; i++)
    combs[i] = (size_t *)malloc(k*sizeof(size_t));

  /* Setup comb for the initial combination */
  for (i=0; i<k; ++i)
    comb[i] = i;

  /* Generate all the other combinations */
  for (i=0; i<ncomb; i++) {
    memcpy(combs[i], comb, k*sizeof(size_t));
    NextComb(comb, k, n);
  }

  *combs_ref = combs;
  *ncomb_ref = ncomb;

  return;
}

size_t EstimateSample(double cov, double probthresh) {
  size_t i = 0;
  double prob = 1;

  if (probthresh == 0)
    return -1;
  while (prob>probthresh)
    prob = pow((1-cov), (double)(++i));

  return i;
}

size_t CountInd(char** ind, size_t nout, KChunk ***chunks) {
  size_t j;
  size_t i=0, sum=0;

  for (i=0; i<nout; i++) {
    j = chunks[i][0]->trainlen;
    while (j--)
      sum += ind[i][j];
  }
  return sum;
}

void Ind1EqInd1ANDInd2(char** ind1, char** ind2, size_t nout, KChunk ***chunks) {
  size_t j;
  size_t i=0;

  for (i=0; i<nout; i++) {
    j = chunks[i][0]->trainlen;
    while (j--)
      ind1[i][j] = ind1[i][j] && ind2[i][j];
  }
  return;
}
