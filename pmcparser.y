/**************************************************************************
          pmcparser.y  -  Parser to parse rules
          --------------------------------------------------------
    begin                : December 4 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.


**************************************************************************/

%{
#define YYDEBUG 1
#include "pmc.h"
extern EdgesCollector *edcoll;
extern CharArray names;
extern size_t *ind;
int yylex(void);
void yyerror(char const *);
int yydebug = 0;

%}

%union {
  size_t i;
  float f;
  char *s;
  Edges *e;
  Cond c;
};

%error-verbose
%token <i> INTEGER
%token <f> FLOATING
%token <s> STRING
%type <i> out inp
%type <f> cov err
%type <c> cond
%type <e> condlist rule
%left LE EQ PERC COV ERR
%nonassoc OUT
%%

finish:
                rulecoll {
                    size_t i;
                    char *buf;
                    Edges *ed;
                    printf("Original rules:\n");
                    for (i=0; i<edcoll->len; i++) {
                        ed = edcoll->ed[i];
                        buf = PrintRules(ed, ed->comb, NULL);
                        printf("%s", buf);
                        free(buf);
                    }
                }

rulecoll:
rulecoll rule '\n' {
                      if (edcoll->len==edcoll->maxlen)
                        edcoll = ResizeEdgesColletor(edcoll, edcoll->maxlen+EDSTEP);
                      edcoll->ed[edcoll->len++] = $2;
}
|                   { edcoll = ResizeEdgesColletor(NULL, EDSTEP); }
;

rule:
                cov err condlist out {
                  Edges *ed = $3;
                  ed->cov = $1;
                  ed->err = $2;
                  ed->nclass = $4;
                  $$ = ed;
                }
                ;

condlist:
                cond {
                  Edges *ed;
                  ed = InitEdges(MAXCONDTEST);
                  ed->ndim = 1;
                  ed->comb[0] = $1.inp;
                  ed->min[$1.inp].i = $1.min;
                  ed->max[$1.inp].i = $1.max;
                  $$ = ed;
                }
                | condlist cond  {
                  Edges *ed = $1;
                  if (++ed->ndim>MAXCONDTEST)
                    Error();
                  ed->comb[ed->ndim-1] = $2.inp;
                  ed->min[$2.inp].i = $2.min;
                  ed->max[$2.inp].i = $2.max;
                  $$ = ed;
                }
                ;

cond:
                INTEGER LE inp LE INTEGER { $$.min = $1; $$.max = $5; $$.inp = $3; }
                | inp LE INTEGER { $$.min = MINVAL; $$.max = $3; $$.inp = $1; }
                | INTEGER LE inp { $$.min = $1; $$.max = MAXVAL; $$.inp = $3; }
                ;
cov:
                FLOATING PERC EQ COV { $$ = $1; }
                ;
err:
                FLOATING PERC EQ ERR { $$ = $1; }
                ;
out:
                OUT EQ INTEGER { $$ = $3; }
                ;
inp:
//                STRING INTEGER STRING { $$ = $2; free($1); free($3); }
                STRING  { //$$ = ind[FindElChArray(&names, $1)];
                          $$ = FindElChArray(&names, $1);
                          free($1); }


%%

void yyerror(char const *s) {
  fprintf(stderr, "%s\n", s);
  Error();
}
