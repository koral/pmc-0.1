/**************************************************************************
          pmc-crule.c  -  Convert Rules in non discretized format
          --------------------------------------------------------
    begin                : Dec 5 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "pmc.h"

EdgesCollector *edcoll;
CharArray names;
size_t *ind;

const char *argp_program_version =
  "pmc-crule 0.1";
const char *argp_program_bug_address =
  "<andrea_corallo@yahoo.it>";
/* Program documentation. */
static char doc[] =
  "pmc-crule convert rules in non discretized format. \n"
  "By default take discretized rules from standard input and write to "
  "standard output";
/* Description of the arguments we accept. */
static char args_doc[] = "FILE";
/* Options we understand. */
static struct argp_option options[] = {
  {"verbose",  'v', 0,      0,  "Produce verbose output" },
  {"names",  'n', 0,      0,  "Takes input names from line 2" },
  {"separator",'s', "'SEPARATOR_CHAR'",      0,
   "Specify the separator character. Default is comma"},
  {"inputrules",   'i', "FILE", 0,
   "Specify a file to load rules to convert" },
  {"missing",  'm', "'MISSING_STRING'",      0,
   "Specify the missing string. Default is ?"},
  {"output",   'o', "FILE", 0,
   "Output to FILE instead of standard output" },
  { 0 }
};
/* To communicate with ParseOpt. */
typedef struct {
  char *args[1];                /* arg1  */
  int verbose, names;
  char *infile, *outfile, *miss, *sep;
} Arguments;

/* Parse a single option. */
static error_t ParseOpt (int key, char *arg, struct argp_state *state) {
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  Arguments *arguments = state->input;

  switch (key)
  {
  case 'v':
    arguments->verbose = 1;
    break;
  case 'o':
    arguments->outfile = arg;
    break;
  case 'n':
    arguments->names = 1;
    break;
  case 's':
    arguments->sep = arg;
    break;
  case 'm':
    arguments->miss = arg;
    break;
  case 'i':
    arguments->infile = arg;
    break;
  case ARGP_KEY_ARG:
    if (state->arg_num >= 1)
      /* Too many arguments. */
      argp_usage (state);
    arguments->args[state->arg_num] = arg;
    break;
  case ARGP_KEY_END:
    if (state->arg_num < 1)
      /* Not enough arguments. */
      argp_usage (state);
    break;
  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}
/* argp parser. */
static struct argp argp = { options, ParseOpt, args_doc, doc };

int main(int argc, char **argv) {
  char sepc;
  int32 nout;
  Arguments arguments;
  CharArray carray, outnames, longnames;
  size_t i, j, k, ncol, nline, nelem;
  char *type, *buf;
  size_t *noutclass;
  Edges *ed;
  Elem32 *elemv;
  Elem32 **col;
  KChunk ***chunks;

  arguments.names = 0;
  arguments.verbose = 0;
  arguments.outfile = NULL;
  arguments.infile = NULL;
  arguments.sep = ",";
  arguments.miss = "?";

  /* Parse our arguments; every option seen by ParseOpt will
     be reflected in arguments. */
  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  InitCharArrray(&carray);


  if (!strcmp(arguments.sep, "\\t"))
    arguments.sep = "\t";
  sepc = arguments.sep[0];

  chunks = LoadChunks(arguments.args[0], &ncol, &nelem, &noutclass, &nout,
                      &names, 1, &outnames, arguments.verbose);

  LoadHeterogeneousFile(arguments.args[0], &ncol, &type, sepc, &ind, &nline,
                        &longnames, arguments.names, 1, arguments.miss, &col,
                        &carray);

  Parse(arguments.infile);

  nline = nline-arguments.names-1;
  elemv = (Elem32 *)malloc((nline)*sizeof(Elem32));

  /* convert rules */

  for (j=0; j<ncol; j++) {
    if (type[j]=='f') {
      memcpy(elemv, col[ind[j]], nline*sizeof(Elem32));
      qsort(elemv, nline, sizeof(Elem32), CmpFloat);
      for (i=0; i<edcoll->len; i++) {
        ed = edcoll->ed[i];
        for (k=0; k<ed->ndim; k++)
          if (ed->comb[k]==j) {
            ed->type[j] = FLOAT;
            if (ed->min[j].i!=MINVAL)
              ed->min[j].f = FindNEl((float *)elemv, ed->min[j].i);
              /* elemv[ed->min[j].i]; */
            if (ed->max[j].i!=MAXVAL)
              ed->max[j].f = FindNEl((float *)elemv, ed->max[j].i);
              /* ed->max[j] = elemv[ed->max[j].i]; */
          }
      }
    }
  }
  printf("New rules:\n");
  for (i=0; i<edcoll->len; i++) {
    ed = edcoll->ed[i];
    buf = PrintRules(ed, ed->comb, &names);
    printf("%s", buf);
    free(buf);
  }


  FreeEdgesCollector(edcoll);
  FreeCharArray(&carray);
  FreeCharArray(&names);
  FreeCharArray(&longnames);
  free(type);
  free(ind);


  return 0;
}
