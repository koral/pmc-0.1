/**************************************************************************
          pmcmapred.c  -  Map Reduce algorithm interface
          --------------------------------------------------------
    begin                : Nov 8 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "pmc.h"

void PmcMap(char *charp) {
  int32 nout;
  double mincov, maxerrperc, validperc;
  size_t i, j, ncomb;// nrow;
  CharArray names;
  size_t *comb, *errmaxv, *noutclass, *fakecomb;
  Rules *rules;
  Edges *limits;
  char **cindext;
  KChunk ***chunks;
  EdgesCollector *edcoll = NULL;

  /* int a=1; */

  /* while(a) */
  /*   ; */

  printf("%s", charp);
  /* nrow = atol(charp); */
  ShiftCharP(&charp);
  nout = atoi(charp);
  ShiftCharP(&charp);
  ncomb = atol(charp);
  ShiftCharP(&charp);
  comb = (size_t *)alloca(ncomb*sizeof(size_t));
  for (i=0; i<ncomb; i++) {
    comb[i] = atol(charp);
    ShiftCharP(&charp);
  }
  noutclass = (size_t *)alloca(ncomb*sizeof(size_t));
  for (i=0; i<nout; i++) {
    noutclass[i] = atol(charp);
    ShiftCharP(&charp);
  }
  maxerrperc = atof(charp);
  ShiftCharP(&charp);
  mincov = atof(charp);

  InitVoidChunks(&chunks, ncomb, nout);
  for (i=0; i<nout; i++)
    for (j=0; j<ncomb; j++)
      LoadChunkFromFile(&chunks[i][j], i, comb[j]);
  limits = InitEdges(ncomb);
  rules = InitRules(nout);
  InitCharArrray(&names);
  FindLimits(chunks, limits, ncomb, nout);
  errmaxv = (size_t *)alloca(nout*sizeof(size_t));
  validperc = 100*(double)(chunks[0][0]->len-chunks[0][0]->trainlen)/
    chunks[0][0]->len;
  for (i=0; i<nout; i++)
    errmaxv[i] = (size_t)(maxerrperc*(noutclass[i]/100)*
                          ((100-validperc)/100));

  edcoll = ResizeEdgesColletor(NULL, EDSTEP);
  fakecomb = (size_t *)alloca(ncomb*sizeof(size_t));
  for (i=0; i<ncomb; i++)
    fakecomb[i] = i;

  cindext = LoadChunkIndFromFile(chunks, nout);
  FindRules(limits, nout, ncomb, fakecomb, chunks, mincov, &edcoll, ncomb,
            errmaxv, cindext);
  FreeChunksInd(cindext, nout);

  // put the right comb in the rules
  for (i=0; i<edcoll->len; i++)
    memcpy(edcoll->ed[i]->comb, comb, ncomb*sizeof(size_t));

  for (i=0; i<edcoll->len; i++) {
    char *buf;
    Edges *ed;

    ed = edcoll->ed[i];
    buf = PrintRules(ed, fakecomb, NULL);
    AppendRule(ed->nclass, ed->ndim, buf, &rules[ed->nclass]);
  }


  // log rules
#ifdef HADOOP
  HadoopEmitRule(rules, comb, ncomb, nout);
#else
  LogRulesToHd(rules, comb, ncomb, nout);
#endif
  FreeEdgesCollector(edcoll);
  FreeRules(rules, nout);
  FreeEdges(limits);
  FreeChunks(chunks, ncomb, noutclass, nout);

  return;
}
