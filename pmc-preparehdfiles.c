/**************************************************************************
          pmc-preparehdfiles.c  -  Prepare files to be cached in hadoop
          --------------------------------------------------------
    begin                : Nov 7 2013
    copyright            : (C) 2013 by Andrea Corallo
    email                : andrea_corallo@yahoo.it

    This file is part of PMC.

    PMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PMC.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "pmc.h"

const char *argp_program_version =
  "pmc-preparehdfiles 0.1";
const char *argp_program_bug_address =
  "<andrea_corallo@yahoo.it>";
/* Program documentation. */
static char doc[] =
  "pmc-preparehdfiles take a discretized dataset and prepare the files for"
  " the hadoop run";
/* Description of the arguments we accept. */
static char args_doc[] = "[FILE]";
/* Options we understand. */
static struct argp_option options[] = {
  {"probability",  'p', "NUM",      0,  "The rule detection probability. Default value: "
   STR(RULEPROBDECT)},
  {"quiet",    'q', 0,      0,  "Don't produce any progress output" },
  {"mincond",    'm', "NUM",      0,  "Minumum number of condition for "
   "rules. Default value: "
   STR(MINCONDDEFAULT)},
  {"maxcond",    'd', "NUM",      0,  "Maximum number of condition for "
   "rules generated. Default value is the number of the input "
   "attributes."},
  {"errormax",    'e', "PERC",      0,  "Maximum error accepted for "
   "the rules generated expressend in percentage. Default value: "
   STR(MAXERRDEFAULT)},
  {"mincovering",    'c', "PERC",      0,  "Minimum covering accepted "
   "for rules expressend in percentage. Default value: "
   STR(MINCOVDEFAULT)},
  { 0 }
};
/* To communicate with ParseOpt. */
typedef struct {
  char *args[1];                /* arg1  */
  char autodim;
  int silent, ncondmax, ncondmin;
  double mincov, maxerrperc, probability;
  char *outfile;
} Arguments;
/* Parse a single option. */
static error_t ParseOpt (int key, char *arg, struct argp_state *state) {
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  Arguments *arguments = state->input;

  switch (key)
  {
  case 'p':
    arguments->probability = 1-atof(arg);
    break;
  case 'm':
    arguments->ncondmin = atoi(arg);
    break;
  case 'd':
    arguments->ncondmax = atoi(arg);
    break;
  case 'e':
    arguments->maxerrperc = atof(arg);
    break;
  case 'c':
    arguments->mincov = atof(arg);
    break;
  case 'a':
    arguments->autodim = 1;
    break;
  case 'q':
    arguments->silent = 1;
    break;

  case ARGP_KEY_ARG:
    if (state->arg_num >= 2)
      /* Too many arguments. */
      argp_usage (state);

    arguments->args[state->arg_num] = arg;

    break;

  case ARGP_KEY_END:
    if (state->arg_num < 0)
      /* Not enough arguments. */
      argp_usage (state);
    break;

  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}
/* Our argp parser. */
static struct argp argp = { options, ParseOpt, args_doc, doc };

int main(int argc, char **argv) {
  double mincov, maxerrperc;
  size_t i, j, ncol, nelem, nextract, ncondmax, ncomb, chunksize;
  int32 nout;
  CharArray names, outnames;
  char filename[MAXSTRING];
  Arguments arguments;
  FILE *pfile;
  size_t *noutclass;
  char **cind;
  size_t **combs;
  KChunk ***chunks;

  arguments.probability = 1-RULEPROBDECT;
  arguments.silent = 0;
  arguments.mincov = MINCOVDEFAULT;
  arguments.maxerrperc = MAXERRDEFAULT;
  arguments.ncondmin = MINCONDDEFAULT;
  arguments.ncondmax = INT32_MAX;
  arguments.autodim = 0;

  /* Parse our arguments; every option seen by ParseOpt will
     be reflected in arguments. */
  argp_parse(&argp, argc, argv, 0, 0, &arguments);


  chunks = LoadChunks(arguments.args[0], &ncol, &nelem, &noutclass,
                      &nout, &names, 0, &outnames, !arguments.silent);
  ncol--;

  ncondmax = arguments.ncondmax;
  if (ncondmax == INT32_MAX)
    ncondmax = ncol;
  mincov = arguments.mincov;
  maxerrperc = arguments.maxerrperc;
  SetTrValChunks(chunks, ncol, nout, 0);

  /* write data chunks */

  for (i=0; i<nout; i++)
    for (j=0; j<ncol; j++) {
      sprintf(filename, STR(TEMPFILEDIR)
              STR(CHUNKFILENAME), (int32)i, j);
      pfile = fopen(filename, "wb");
      if (!pfile)
        Error();
      chunksize = (chunks[i][j]->len-1)*sizeof(int32)+sizeof(KChunk);
      fwrite (chunks[i][j] , 1, chunksize, pfile);
      fclose (pfile);
    }

  /* write input file */
  pfile = fopen (STR(TEMPFILEDIR)
                 STR(HDINPUTFILENAME), "wb");
  for (nextract=1; nextract<=ncondmax; nextract++) {
    Combs(&combs, &ncomb, ncol, nextract);

    for(i=0; i<ncomb; i++) {
      fprintf(pfile, "%ld ", ncol);
      fprintf(pfile, "%d ", nout);
      fprintf(pfile, "%ld ", nextract);
      for(j=0; j<nextract; j++)
        fprintf(pfile, "%ld ", combs[i][j]);
      for(j=0; j<nout; j++)
        fprintf(pfile, "%ld ", noutclass[j]);
      fprintf(pfile, "%f ", maxerrperc);
      fprintf(pfile, "%f \n", mincov);
    }
    FreeCombs(combs, ncomb);
  }
  fclose(pfile);

  cind = InitChunksInd(nout, chunks,
                       EstimateSample(mincov/100,
                                      arguments.probability));

  /* write index files */

  for (i=0; i<nout; i++) {
    sprintf(filename, STR(TEMPFILEDIR)
            STR(CHUNKINDFILENAME), (int32)i);
    pfile = fopen(filename, "wb");
    if (!pfile)
      Error();
    fwrite(cind[i], 1, chunks[i][0]->len, pfile);
    fclose(pfile);
  }

  FreeChunks(chunks, ncol, noutclass, nout);
  free(noutclass);

  return 0;
}
