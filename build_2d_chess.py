#! /usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

from random import randint

MINVAL = 1

def PrepareSet():
    print("Write a file with an N dimentional dataset with an hidden chessboard in 2 dimension")
    ncol = int(raw_input("Enter the number of N: "))
    nelem = int(raw_input("Enter the number of elements in the dataset: "))
    MAXVAL = int(raw_input("Enter the number of max different values in every col: "))
    err = float(raw_input("Enter the noise percentage in the dataset: "))
    n1 = int(raw_input("Enter the number of the first dim: "))
    n2 = int(raw_input("Enter the number of the second dim: "))
    name = "datasets/file"+str(nelem)+"p_"+str(MAXVAL)+"v_"+str(ncol)+"dim_"+str(int(err))+"err.int"
    arr0 = np.zeros((nelem/2, ncol), dtype=np.int)
    for k in range(nelem/2):
        for i in range(ncol):
            if i!=n2 :
                arr0[k][i] = randint(MINVAL, MAXVAL)
            else:
                if randint(0, 100)>=err:
                    if arr0[k][n1] > MAXVAL/2:
                        arr0[k][n2] = randint(MAXVAL/2, MAXVAL)
                    else :
                        arr0[k][n2] = randint(MINVAL, MAXVAL/2)
                else :
                    arr0[k][i] = randint(MINVAL, MAXVAL)

    arr1 = np.zeros((nelem/2, ncol), dtype=np.int)
    for k in range(nelem/2):
        for i in range(ncol):
            if i!=n2 :
                arr1[k][i] = randint(MINVAL, MAXVAL)
            else:
                if randint(0, 100)>=err:
                    if arr1[k][n1] <= MAXVAL/2:
                        arr1[k][n2] = randint(MAXVAL/2, MAXVAL)
                    else :
                        arr1[k][n2] = randint(MINVAL, MAXVAL/2)
                else :
                    arr1[k][i] = randint(MINVAL, MAXVAL)

    # plt.scatter(arr0[:,n1], arr0[:,n2], c='b', s=100)
    # plt.scatter(arr1[:,n1], arr1[:,n2], c='r', s=100)
    # plt.show()
    return arr0, arr1, name

def SaveSet(m0, m1, name):
    f = open(name,'w')
    for i in m0[0]:
        f.write('i\t')
    f.write('o\n')
    for i in range(len(m0[0])):
        f.write('Inp'+str(i)+'#'+str(i)+'#i\t')
    f.write('OUT\n')
    for patt in m0:
        for i in patt:
            f.write(str(i)+'\t')
        f.write('0\n')
    for patt in m1:
        for i in patt:
            f.write(str(i)+'\t')
        f.write('1\n')
    f.close()

if __name__ == "__main__":
    m0, m1, name = PrepareSet()
    #name = raw_input("Entern the name of the output file: ")
    SaveSet(m0, m1, name)
